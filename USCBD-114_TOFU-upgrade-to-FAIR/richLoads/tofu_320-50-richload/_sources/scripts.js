


// CURRENT CREATIVE DOM ELEMENT WITH DYNAMIC CONTENT
//Add and remove as you need.
var bgVideo,
    footer,
    txt_1,
    txt_4,
    txt_5,
    txt_6,
    txt_7,
    bubble

// STANDARD VARIABLES USUALY NO NEED TO TOUCH
var uscLogo,
    uscLogoWhite,
    container,
    ctaBtn,
    ctaText,
    legalText,
    legalIAText,
    legalDetails,
    legalDetailsText,

    ctaArrow,
    timeline,
    thisCarousel,

    activateCarousel = (myFT.instantAds.carouselDeviceTotal > 0),
    isLegalHidden = true,
    animInited = false,
    adSize,
    isWebkit = navigator.userAgent.indexOf('AppleWebKit') != -1,

    extrudeDuration = 1,
    letterDelay = 0.1


TweenMax.defaultEase = Sine.easeInOut


var timeline = new TimelineMax({ paused: true, smoothChildTiming: true, onComplete: onSequenceCompleted })

var maxWidth = 165
function setClip(el) {
    var elDim = el.getBoundingClientRect()
    var clipDiv = document.createElement("div")

    clipDiv.style.position = "absolute"
    clipDiv.style.zIndex = 4
    clipDiv.id = el.id + "_clip"
    clipDiv.classList.add("cliper")

    clipDiv.style.width = elDim.width + "px"
    clipDiv.style.maxWidth = maxWidth + "px"
    clipDiv.style.height = elDim.height + "px"
    clipDiv.style.top = elDim.top + "px"
    clipDiv.style.left = elDim.left + "px"
    clipDiv.style.background = "#0065A4"

    el.after(clipDiv)
}

function initAnimation() {

    // Odometer timing
    let AnimTime = 11   //7
    let odometer = document.querySelector("#counter-container")
    // Sub-function to get the band heights
    function getHeight(el) {
        return -document.querySelector(el).getBoundingClientRect().height + 114
    }

    // Cliping effect in bubble...
    setClip(document.querySelector("#txt_1"))
    setClip(document.querySelector("#txt_4"))

    let NINE = document.querySelectorAll("#b_2>.digit_item")[1]
    NINE.style.opacity = 0

    timeline

        .set(uscLogo, { x: 14, y: 9, scaleX: 0.95 })
        .set(uscLogoWhite, { x: 25, y: 19, scaleX: 0.98 })
        .set(footer, { x: -190, autoAlpha: 1 })
        .set(bubble, { autoAlpha: 0, x: -16, scaleX: 1.15 })
        .set([txt_1, txt_4], { autoAlpha: 0 })
        .set(ctaBtn, { autoAlpha: 0, boxShadow: "0px 2px 8px -2px rgba(0,0,0,0.0)" })
        .set(bg, { autoAlpha: 1 })
        .set(odometer, { autoAlpha: 0, scale: 0.18, y: -55, x: -165 })
        .set(line_1, { autoAlpha: 0, scale: 0, transformOrigin: "center center" }, "lockoutFrame+=1.1")
        .set(document.querySelectorAll(".cliper"), { autoAlpha: 0 })

        //NO TWEEN BEFORE THIS LINE. IT WON'T BE SEEN, SO USE .set
        .set(container, { autoAlpha: 1 })

        .add("firstFrame", "+=.25")

        //  USC logo enters... Then bubble and text animates
        .to(uscLogoWhite, 0, { autoAlpha: 1 }, "firstFrame")
        .to(bubble, .6, { autoAlpha: 1, x: 0 }, "firstFrame+=0.6")




        // Text in bubble, line by line
        .to(document.querySelectorAll(".cliper"), 0, { autoAlpha: 1 }, "firstFrame+=1.2")
        .to([txt_1, txt_4], 0, { autoAlpha: 1 }, "firstFrame+=1.2")

        .to(document.querySelector("#txt_1_clip"), .8, { width: "0px", x: maxWidth, transformOrigin: "right 50%" }, "firstFrame+=1.2")
        .to(document.querySelector("#txt_4_clip"), .4, { width: (maxWidth - 97) + "px", x: 97, transformOrigin: "right 50%" }, "firstFrame+=2.0")

        // really?
        .to(document.querySelector("#txt_4_clip"), .6, { width: "0px", x: maxWidth, transformOrigin: "right 50%" }, "firstFrame+=3.1")



        .add("secondFrame", "+=1")
        // Hide first frame
        .to([txt_1, txt_4, bubble, uscLogoWhite], .35, {
            autoAlpha: 0,
            ease: Power2.easeOut
        }, "secondFrame")

        .add("odometerFrame", "secondFrame+=1.5")

        // Odometer container movements
        .to(odometer, 0.55, { autoAlpha: 1 }, "odometerFrame")

        .add("numberRoll", "odometerFrame+=0.1")

        .to("#b_2", (AnimTime / 5), { top: getHeight("#b_2"), ease: Power1.easeOut }, "numberRoll+=1")
        .to(NINE, 0.4, { opacity: 1 }, "numberRoll+=1.1")



        .to("#b_3", (AnimTime / 2), { top: getHeight("#b_3"), ease: Power1.easeOut }, "numberRoll+=1.2")
        .to("#b_4", AnimTime, { top: getHeight("#b_4"), ease: Power1.easeOut }, "numberRoll")
        .to("#b_5", AnimTime, { top: getHeight("#b_5"), ease: Power1.easeOut }, "numberRoll")

        .to("#b_6", AnimTime, { top: getHeight("#b_6"), ease: Power1.easeOut }, "numberRoll")
        .to("#b_7", AnimTime, { top: getHeight("#b_7"), ease: Power1.easeOut }, "numberRoll")
        .to("#b_8", AnimTime, { top: getHeight("#b_8"), ease: Power1.easeOut }, "numberRoll")


        // odometer finished


        .to([txt_5, txt_6], .55, { autoAlpha: 1, x: 0, ease: Power3.easeOut }, "odometerFrame")
        .to(footer, .15, { x: 0, ease: Power1.easeOut }, "secondFrame+=.7")


        .to(uscLogo, .85, { autoAlpha: 1 }, "secondFrame+=.75")
        .to(ctaBtn, .15, { autoAlpha: 1, x: 0, ease: Power3.easeOut }, "secondFrame+=.95")

        .add("lockoutFrame", "-=4.2")

        .set(line_1, { autoAlpha: 1 }, "lockoutFrame+=2.7")
        .to(line_1, .65, { scale: 1, ease: Power2.easeIn }, "lockoutFrame+=2.75")


        .add("endFrame", "+=0.25")

        .to(legalText, .6, { autoAlpha: 1, ease: Power2.easeOut }, "endFrame")


        // Reveal snipe if needed
        .call(function () {

            if (myFT.instantAds.showSnipe != "yes")
                return;

            var snipeTL = new TimelineMax();

            if (snipe.style.display == "none") {
                snipe.style.display = "flex";
                snipeTL.to(snipe, 0.35, { autoAlpha: 1 }, "endFrame+=0")
                // .from(snipe, 0.25, {scale: 1.45}, "endFrame+=0")
            }
            return snipeTL;
        }, [], this, "endFrame+=0.1")


        //auto play carousel at end
        // .call(function () {
        //     if (thisCarousel.deviceTotal() > 1)
        //         thisCarousel.play();
        // }, this, "endFrame+=1")

        .call(onOverCTA, [], this, "endFrame+=1.6")

        .play()
}





/*------------------------------------------------------------------------*/


window.onload = function () {
    fillDynamicContent();
    TweenMax.delayedCall(.4, initAnimation);
}



//WHEN SOMETHING MUST BE DONE AFTER THE ANIMATION.
function onSequenceCompleted() {
    // Good place to start the rollover listener so it does not
    // take place in the middle of the animation
    container.onmouseenter = onOverCTA
}

// DEFINE ROLL OVER EFFECTS
function onOverCTA() {

    TweenMax.set(ctaBtn, { transformOrigin: "center bottom" })
    TweenMax.set(cta_gradient, { x: -30, force3D: false })

    TweenMax.to(ctaBtn, 0.2, { scale: 1.08, boxShadow: "6px 16px 10px -8px rgba(0,0,0,0.2)", force3D: false, })
    TweenMax.to(ctaBtn, 0.2, { scale: 1, boxShadow: "0px 2px 8px -2px rgba(0,0,0,0.0)", force3D: false, delay: .4 })
    TweenMax.to(cta_gradient, 0.6, { x: "+=300", ease: Power0.easeNone, force3D: false, delay: .15 })
}


/*------------------------------------------------------------------------*/

function fillDynamicContent() {


    // CURRENT CREATIVE DOM ELEMENT WITH DYNAMIC CONTENT
    //Add and remove as you need.
    bgVideo = getAssets("#bgVideo")
    txt_1 = getAssets("#txt_1")
    txt_4 = getAssets("#txt_4")
    txt_5 = getAssets("#txt_5")
    txt_6 = getAssets("#txt_6")
    txt_7 = getAssets("#txt_7")
    bubble = getAssets("#bubble")
    footer = getAssets("#footer")



    // STANDARD VARIABLES USUALY NO NEED TO TOUCH
    uscLogo = getAssets("#uscLogo")
    uscLogoWhite = getAssets("#uscLogoWhite")
    container = getAssets("#container")
    ctaBtn = getAssets("#cta_btn")
    ctaText = getAssets("#cta_text")
    legalText = getAssets("#legal_text")
    legalIAText = getAssets("#legal_ia_text")
    legalDetails = getAssets("#legal_details")
    legalDetailsText = getAssets("#legal_details_text")

    adSize = { w: container.offsetWidth, h: container.offsetHeight }


    //TEXT FIELDS
    var textFields = [txt_1, txt_4, txt_5, txt_6, txt_7]

    //IMAGE AS DIV BACKGROUND
    var imageFields = [uscLogo, bubble]

    var svgNodes = []



    //LINES
    var lines = document.querySelectorAll(".line")

    // DYNAMIC IA SETUP
    // populate instantAd components
    myFT.applyClickTag(container, 1, myFT.instantAds.clickTagURL)

    myFT.applyButton(container, function () {
        myFT.tracker("bannerClick", "banner container clicked")
    })

    // Click Listeners on legal assets
    myFT.applyButton(legalText, function () {
        if (isLegalHidden) {
            showLegal();
        } else {
            hideLegal();
        }
    });

    myFT.applyButton(legalDetails, function () {
        hideLegal();
    });

    // CTA BUTTON
    var ctaBtnProps = splitValues(myFT.instantAds.ctaSizePosZ)
    ctaBtn.style.width = ctaBtnProps[0]
    ctaBtn.style.height = ctaBtnProps[1]
    ctaBtn.style.left = ctaBtnProps[2]
    ctaBtn.style.top = ctaBtnProps[3]
    ctaBtn.style.zIndex = ctaBtnProps[4]

    // CTA BUTTON TEXT
    var ctaTxtProps = splitValues(myFT.instantAds.ctaTextProps)
    ctaText.innerHTML = myFT.instantAds.cta
    ctaText.style.fontSize = ctaTxtProps[0]
    ctaText.style.lineHeight = ctaTxtProps[1]
    ctaText.style.color = ctaTxtProps[2]
    ctaText.style.textAlign = ctaTxtProps[3]
    ctaText.style.left = ctaTxtProps[4]
    ctaText.style.paddingTop = ctaTxtProps[5]


    if (myFT.instantAds.addCtaArrow == 'yes') {
        ctaText.innerHTML = ctaText.innerHTML + '<svg id="ctaArrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.99 20.7"><polygon class="cls-1" fill="' + ctaTxtProps[2] + ' " points="3.45 20.7 0 17.24 7.1 10.19 0.31 3.43 3.73 0 13.99 10.21 3.45 20.7"/></svg>'
        ctaArrow = getAssets("#ctaArrow")
        var arrowW = parseInt(ctaTxtProps[0].replace("px", "") - +2)
        var arrowH = Math.ceil(parseFloat(ctaTxtProps[0].replace("px", "")) * .75)
        ctaArrow.style.width = arrowW + "px"
        ctaArrow.style.height = arrowH + "px"
        ctaArrow.style.left = (arrowW * .15) + "px"
    }



    //TEXTS
    // Set all textField properties with manifest value
    for (var i = 0; i < textFields.length; ++i) {

        var assetName = (textFields[i].getAttribute("data-ia") || textFields[i].id).toString()
        var props = splitValues(myFT.instantAds[assetName + '_Props'])
        textFields[i].innerHTML = myFT.instantAds[assetName]
        textFields[i].style.fontSize = props[0]
        textFields[i].style.lineHeight = props[1]
        textFields[i].style.color = props[2]
        textFields[i].style.textAlign = props[3]
        textFields[i].style.left = props[4]
        textFields[i].style.top = props[5]
    }


    /*------------------------------------------------------------------------*/

    //IMAGE AS DIV BACKGROUND
    // Set all properties of all image (use div with background image)
    for (var i = 0; i < imageFields.length; ++i) {

        var assetName = (imageFields[i].getAttribute("data-ia") || imageFields[i].id).toString()
        var props = splitValues(myFT.instantAds[assetName + 'SizePosZ'])

        imageFields[i].style.backgroundImage = "url(" + myFT.instantAds[assetName] + ")"
        imageFields[i].style.width = props[0]
        imageFields[i].style.height = props[1]
        imageFields[i].style.left = props[2]
        imageFields[i].style.top = props[3]
        imageFields[i].style.zIndex = props[4]

        if (assetName == 'uscLogo' && uscLogoWhite != undefined) {
            //match logo with white logo
            uscLogoWhite.style.backgroundImage = "url(" + myFT.instantAds[assetName] + ")";
            uscLogoWhite.style.width = props[0];
            uscLogoWhite.style.height = props[1];
            uscLogoWhite.style.left = props[2];
            uscLogoWhite.style.top = props[3];
            uscLogoWhite.style.zIndex = props[4];
        }

    }
    /*------------------------------------------------------------------------*/

    //INSERT SVG NODE SO IT CAN BE MANIPULATED
    // Set all properties of all image (use div with background image)
    for (var i = 0; i < svgNodes.length; ++i) {

        var assetName = (svgNodes[i].getAttribute("data-ia") || svgNodes[i].id).toString()
        var props = splitValues(myFT.instantAds[assetName + 'SizePosZ'])

        svgNodes[i].innerHTML = myFT.instantAds[assetName]

        svgNodes[i].style.width = props[0]
        svgNodes[i].style.height = props[1]
        svgNodes[i].style.left = props[2]
        svgNodes[i].style.top = props[3]
        svgNodes[i].style.zIndex = props[4]

    }

    /*------------------------------------------------------------------------*/


    if (bgVideo != undefined) {
        bgVideo.pause();
        bgVideo.currentTime = 0;

        //VIDEOS
        var props = splitValues(myFT.instantAds.bgVideoSizePosZColor)

        bgVideo.style.width = props[0]
        bgVideo.style.height = props[1]
        bgVideo.style.left = props[2]
        bgVideo.style.top = props[3]
        bgVideo.style.zIndex = props[4]
        bgVideo.style.backgroundColor = props[5]
    }

    /*------------------------------------------------------------------------*/

    // LEGAL TEXT & BUTTON
    var legalTxtProps = splitValues(myFT.instantAds.legalTextProps)
    legalIAText.innerHTML = myFT.instantAds.legal
    legalIAText.style.fontSize = legalTxtProps[0]
    legalIAText.style.lineHeight = legalTxtProps[1]
    legalIAText.style.color = legalTxtProps[2]
    legalIAText.style.left = legalTxtProps[4]
    legalIAText.style.top = legalTxtProps[5]
    legalText.style.textAlign = legalTxtProps[3]

    var legalTextSizeProps = splitValues(myFT.instantAds.legalSizePosZ)
    legalText.style.width = legalTextSizeProps[0]
    legalText.style.height = legalTextSizeProps[1]
    legalText.style.left = legalTextSizeProps[2]
    legalText.style.top = legalTextSizeProps[3]
    legalText.style.zIndex = legalTextSizeProps[4]

    // // check to see if there are legal details that would display on click
    if (myFT.instantAds.legalDetails != "") {
        legalDetails.style.display = "inline"
        legalDetailsText.innerHTML = myFT.instantAds.legalDetails
    }
    /*------------------------------------------------------------------------*/

    //SET LINES PROPS

    if (lines != null && lines != undefined) {
        // Set all properties of all lines
        for (var i = 0; i < lines.length; i++) {

            var assetName = lines[i].id.toString()
            var props = splitValues(myFT.instantAds[assetName + '_SizePosZColor'])
            lines[i].style.width = props[0]
            lines[i].style.height = props[1]
            lines[i].style.left = props[2]
            lines[i].style.top = props[3]
            lines[i].style.zIndex = props[4]
            lines[i].style.backgroundColor = props[5]
        }
    }
}


/*----- UTILS METHOD -------------------------------------------------------------------*/

function splitValues(values) {
    return values.split(',')
}

function getAssets(cssPointer) {
    // Return undefined, or a direct reference
    // if only one match or the array of matches.
    var a = myFT.$(cssPointer)
    var returnVal = undefined
    if (a != undefined && a.length > 0)
        returnVal = a.length == 1 ? a[0] : a
    return returnVal
}

function showLegal() {
    TweenMax.to(legalDetails, 0.10, { autoAlpha: 1 })
    myFT.tracker("legalClick", "show legal")
    isLegalHidden = false
}

function hideLegal() {
    TweenMax.to(legalDetails, 0.10, { autoAlpha: 0 })
    myFT.tracker("legalClick", "hide legal")
    isLegalHidden = true
}


var Carousel = {}

if (activateCarousel) {
    // Create the carousel object
    Carousel.init = function () {

        var deviceCarousel = getAssets("#device_carousel"),
            devicesContainer = getAssets("#device_sets_container"),
            carousel_logo_holder = getAssets("#carousel_logo_holder"),
            carouselControls = getAssets(".carousel-controls"),
            carouselLeft = getAssets("#carousel_left"),
            carouselRight = getAssets("#carousel_right"),
            carouselDeviceImages = [],
            carouselDeviceLogos = [],
            carouselDeviceLinks = [],
            devices = [],
            logos = [],
            carouselLogosProps,
            carouselPaused = true,
            deviceTotal = null,
            currentDeviceNum = 0,
            carouselOffset,
            logosOffset,
            autoPlayCancelled = false,
            onCreated = function () {

                deviceTotal = myFT.instantAds.carouselDeviceTotal
                var carouselProps = splitValues(myFT.instantAds.carouselSizePos)
                deviceCarousel.style.width = carouselProps[0]
                deviceCarousel.style.height = carouselProps[1]
                deviceCarousel.style.left = carouselProps[2]
                deviceCarousel.style.top = carouselProps[3]
                deviceCarousel.style.cursor = "pointer"


                // Remove the arrow if showing only one device or
                // activateCarouselArrow = 'no'
                if (deviceTotal > 1 && (activateCarouselArrow == "yes")) {
                    carouselLeft.addEventListener("click", onLeftClick)
                    carouselRight.addEventListener("click", onRightClick)
                } else {
                    carouselControls[0].style.display = "none"
                    carouselControls[1].style.display = "none"
                }

                carouselOffset = devicesContainer.offsetWidth

                carouselLogosProps = splitValues(myFT.instantAds.carouselLogosSizePos)
                carousel_logo_holder.style.width = carouselLogosProps[0]
                carousel_logo_holder.style.height = carouselLogosProps[1]
                carousel_logo_holder.style.left = carouselLogosProps[2]
                carousel_logo_holder.style.top = carouselLogosProps[3]

                logosOffset = parseInt(carouselLogosProps[0].replace('px', ''))


                //Retrieve carousel config from InstantAds data
                for (var i = 1; i <= deviceTotal; i++) {

                    var imgSearch = /\.(?:jpg|gif|png|svg)/gi
                    var deviceSrcImg = myFT.instantAds["carousel_img_" + i]
                    var deviceLogoSrcImg = myFT.instantAds["carousel_logo_" + i]
                    var deviceClickTag = myFT.instantAds["carousel_url_" + i]
                    carouselDeviceImages.push(deviceSrcImg)
                    carouselDeviceLinks.push(deviceClickTag)

                    var imgExist = imgSearch.test(deviceLogoSrcImg)
                    carouselDeviceLogos.push(imgExist ? deviceLogoSrcImg : 'none')
                }


                //Create new HTML for the carousel
                for (var i = 0; i < deviceTotal; i++) {

                    var device = document.createElement("div")
                    device.id = "device" + (i + 1)
                    device.className = "device"
                    device.style.backgroundImage = 'url(' + carouselDeviceImages[i] + ')'

                    if (deviceTotal > 1)
                        myFT.applyClickTag(device, (2 + i), carouselDeviceLinks[i])
                    devices.push(device)

                    devicesContainer.appendChild(device)

                    TweenMax.set(device, { x: carouselOffset })

                    if (carouselDeviceLogos[i - 1] != 'none') {
                        var logoImg = document.createElement("div")
                        logoImg.id = "device_logo" + (i + 1)
                        logoImg.className = "devicelogo"
                        logoImg.style.backgroundImage = 'url(' + carouselDeviceLogos[i]; +')'
                        carousel_logo_holder.appendChild(logoImg)
                        logos.push(logoImg)
                        TweenMax.set(logoImg, { x: logosOffset })
                    }

                }

                function onLeftClick(evt) {
                    myFT.tracker("leftCarouselClick", "clicking on carousel left arrow")
                    evt.stopPropagation()
                    carouselPaused = true
                    clearAutoPlay()
                    goLeft()
                }

                function onRightClick(evt) {
                    myFT.tracker("rightCarouselClick", "clicking on carousel right arrow")
                    evt.stopPropagation()
                    carouselPaused = true
                    clearAutoPlay()
                    goRight()
                }

                TweenMax.to([devices[0], logos[0]], 0.3, { x: 0 })

            },

            goLeft = function () {
                TweenMax.killAll()
                var dOut = devices[currentDeviceNum]
                var lOut = logos[currentDeviceNum]
                if (currentDeviceNum == 0)
                    currentDeviceNum = deviceTotal
                currentDeviceNum--
                var dIn = devices[currentDeviceNum]
                var lIn = logos[currentDeviceNum]

                TweenMax.to(dOut, 0.65, { x: "+=" + carouselOffset, alpha: 0, ease: Power1.easeInOut })
                TweenMax.to(lOut, 0.65, { x: "+=" + logosOffset, alpha: 0, ease: Power1.easeInOut })
                TweenMax.fromTo(dIn, 0.65, { x: -carouselOffset }, { x: 0, alpha: 1, ease: Power1.easeInOut })
                TweenMax.fromTo(lIn, 0.65, { x: -logosOffset }, { x: 0, alpha: 1, ease: Power1.easeInOut })
            },
            goRight = function (stopAutoplay) {
                TweenMax.killAll()
                var dOut = devices[currentDeviceNum]
                var lOut = logos[currentDeviceNum]
                if (++currentDeviceNum == deviceTotal) {
                    currentDeviceNum = 0
                    clearAutoPlay()
                }
                var dIn = devices[currentDeviceNum]
                var lIn = logos[currentDeviceNum]
                TweenMax.to(dOut, 0.65, { x: "-=" + carouselOffset, alpha: 0, ease: Power1.easeInOut })
                TweenMax.to(lOut, 0.65, { x: "-=" + logosOffset, alpha: 0, ease: Power1.easeInOut })
                TweenMax.fromTo(dIn, 0.65, { x: carouselOffset }, { x: 0, alpha: 1, ease: Power1.easeInOut })
                TweenMax.fromTo(lIn, 0.65, { x: logosOffset }, { x: 0, alpha: 1, ease: Power1.easeInOut })
            },
            clearAutoPlay = function () {
                autoPlayCancelled = true
            },
            playCarousel = function () {
                if (carouselPaused || autoPlayCancelled)
                    return

                goRight()
                TweenMax.delayedCall(2.5, playCarousel)
            },
            startCarousel = function () {
                carouselPaused = false
                playCarousel()
            },
            splitValues = function (values) {
                return values.split(',')
            }

        return {
            deviceCarousel: deviceCarousel,
            logos: carousel_logo_holder,
            devicesContainer: devicesContainer,
            controls: { left: carouselLeft, right: carouselRight },
            carouselDeviceImages: carouselDeviceImages,
            carouselDeviceLogos: carouselDeviceLogos,
            carouselDeviceLinks: carouselDeviceLinks,
            carouselPaused: carouselPaused,
            devices: devices,
            deviceTotal: function () { return deviceTotal },
            currentDeviceNum: currentDeviceNum,
            carouselOffset: carouselOffset,
            create: onCreated,
            goLeft: goLeft,
            goRight: goRight,
            clearAutoPlay: clearAutoPlay,
            play: startCarousel
        }
    }
    thisCarousel = Carousel.init(this)
    thisCarousel.create()
}
