function createDiv(id = null, classnames = [], text = "", parent) {
    let div = document.createElement("div")
    if (id) {
        div.id = id
    }
    if (classnames.length) {
        div.classList.add(classnames.join(" "))
    }
    if (text !== "") {
        div.innerText = text
    }
    document.querySelector(parent).append(div)
}

// Containers
createDiv("counter-container", [], "", "#container")
createDiv("score", [], "", "#counter-container")
createDiv("dollarSign", [], "$", "#score")


function createBands(spins, end_num, id) {

    if (id === "b_1") {
        createDiv(null, ["statique"], "1", "#score")
    }

    else {
        let band = document.createElement("div")
        band.classList.add("digit_band")
        let band_in = document.createElement("div")
        band_in.classList.add("digit_band_in")
        band_in.id = id

        // Full spin from zero to zero
        for (i = 0; i < spins * 10; i++) {
            let item = document.createElement("div")
            item.classList.add("digit_item")
            item.innerText = i % 10
            // band_in.append(item)


            if (id === "b_3" && i > 8) {
                band_in.append(item)
            }
            if (id !== "b_3") {
                band_in.append(item)
            }
        }

        // To reach the desired number at the end of the spin
        for (i = 0; i <= end_num; i++) {

            let item = document.createElement("div")
            item.classList.add("digit_item")
            item.innerText = i
            if (id === "b_2" && i !== 0) {
                band_in.append(item)
            }
            if (id !== "b_2") {
                band_in.append(item)
            }

        }

        band.append(band_in)
        document.querySelector("#score").append(band)
    }

}



// Elements for the digits
createBands(0, 1, "b_1")    // 1
createBands(0, 2, "b_2")    // 2
createDiv(null, ["coma"], ",", "#score")
createBands(1, 3, "b_3")    // 3
createBands(2, 2, "b_4")    // 2
createBands(3, 5, "b_5")    // 5
createDiv(null, ["coma"], ",", "#score")
createBands(2, 0, "b_6")    // 0
createBands(4, 2, "b_7")    // 2
createBands(6, 5, "b_8")    // 5


// To monitor the real animation time
let realTime_start = new Date().getTime()
function realTime() {
    let realTime_stop = new Date().getTime()
    // document.querySelector("#diffTime").innerText = (realTime_stop - realTime_start) / 1000 + " seconds"
    console.log((realTime_stop - realTime_start) / 1000 + " seconds")
}

// ===== ANIMATION

// Sub-function to get the band heights
// function getHeight(el) {
//     return -document.querySelector(el).getBoundingClientRect().height + 120
// }

// let anim = new TimelineLite()
// let AnimTime = 4
// anim
//     .add("odometerFrame", "+=0")

//     .to("#b_2", (AnimTime / 2), { top: getHeight("#b_2"), ease: Circ.easeOut }, "odometerFrame+=0.8")

//     .to("#b_3", AnimTime, { top: getHeight("#b_3"), ease: Circ.easeOut }, "odometerFrame")
//     .to("#b_4", AnimTime, { top: getHeight("#b_4"), ease: Circ.easeOut }, "odometerFrame")
//     .to("#b_5", AnimTime, { top: getHeight("#b_5"), ease: Circ.easeOut }, "odometerFrame")

//     .to("#b_6", AnimTime, { top: getHeight("#b_6"), ease: Circ.easeOut }, "odometerFrame")
//     .to("#b_7", AnimTime, { top: getHeight("#b_7"), ease: Circ.easeOut }, "odometerFrame")
//     .to("#b_8", AnimTime, { top: getHeight("#b_8"), ease: Circ.easeOut }, "odometerFrame")

