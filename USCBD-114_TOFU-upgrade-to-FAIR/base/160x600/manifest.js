FT.manifest({
    "filename": "index.html",
    "width": 160,
    "height": 600,
    "clickTagCount": 1,
    "hideBrowsers": ["ie10", "opera"],
    "trackingEvents": [
        { "name": "bannerClick", "type": "string" },
        { "name": "legalClick", "type": "string" }
    ],
    "richLoads": [
        { "name": "richload", "src": "tofu_160-600-richload" }
    ],
    "videos": [
        { "name": "bgVideo", "ref": "127606/empty_video" }
    ],
    "instantAds": [
        {
            "name": "creative",
            "type": "text",
            "default": ""
        },
        {
            "name": "clickTagURL",
            "type": "text",
            "default": "https://www.uscellular.com/plans?utm_campaign=[%campaignName%]&utm_source=ttd&utm_medium=ban&utm_content=[%FT_CREATIVE_ID%]^[%FT_CONFIGURATION_ID%]&utm_tactic=[%FT_PLACEMENT_ID%]"
        },
        {
            "name": "richload",
            "type": "richload",
            "default": "tofu_160-600-richload/index.html"
        },
        {
            "name": "bgVideo",
            "type": "video",
            "default": "127606/empty_video"
        },
        {
            "name": "bgVideoSizePosZColor",
            "type": "text",
            "default": "100%,192px,0px,0px,unset,#fff"
        },
        {
            "name": "txt_1",
            "type": "text",
            "default": "Other"
        },
        {
            "name": "txt_1_Props",
            "type": "text",
            "default": "22px,22px,#FFF,left,21px,213px"
        },
        {
            "name": "txt_2",
            "type": "text",
            "default": "still charge"
        },
        {
            "name": "txt_2_Props",
            "type": "text",
            "default": "22px,22px,#FFF,left,21px,259px"
        },
        {
            "name": "txt_3",
            "type": "text",
            "default": "activation"
        },
        {
            "name": "txt_3_Props",
            "type": "text",
            "default": "22px,22px,#FFF,left,21px,282px"
        },
        {
            "name": "txt_4",
            "type": "text",
            "default": "fees. Really?"
        },
        {
            "name": "txt_4_Props",
            "type": "text",
            "default": "22px,22px,#FFF,left,21px,306px"
        },
        {
            "name": "txt_5",
            "type": "text",
            "default": "We've saved<br>customers"
        },
        {
            "name": "txt_5_Props",
            "type": "text",
            "default": "26.5px,29.5px,#FFFFFF,center,8px,218px"
        },
        {
            "name": "txt_6",
            "type": "text",
            "default": "ON ACTIVATION FEES"
        },
        {
            "name": "txt_6_Props",
            "type": "text",
            "default": "15.4px,15px,#FFF,center,6px,332px"
        },
        {
            "name": "txt_7",
            "type": "text",
            "default": "this year"
        },
        {
            "name": "txt_7_Props",
            "type": "text",
            "default": "13.5px,normal,#FFF,center,53px,352px"
        },
        {
            "name": "txt_8",
            "type": "text",
            "default": "carriers"
        },
        {
            "name": "txt_8_Props",
            "type": "text",
            "default": "22px,22px,#FFF,left,21px,235px"
        },
        {
            "name": "txt_9",
            "type": "text",
            "default": ""
        },
        {
            "name": "txt_9_Props",
            "type": "text",
            "default": "24.5px,23px,#FFF,left,73px,121px"
        },
        {
            "name": "txt_10",
            "type": "text",
            "default": ""
        },
        {
            "name": "txt_10_Props",
            "type": "text",
            "default": "24.5px,23px,#FFF,left,73px,121px"
        },
        {
            "name": "txt_11",
            "type": "text",
            "default": ""
        },
        {
            "name": "txt_11_Props",
            "type": "text",
            "default": "24.5px,23px,#FFF,left,73px,121px"
        },
        {
            "name": "txt_12",
            "type": "text",
            "default": ""
        },
        {
            "name": "txt_12_Props",
            "type": "text",
            "default": "24.5px,23px,#FFF,left,73px,121px"
        },
        {
            "name": "svgNode1",
            "type": "text",
            "default": ""
        },
        {
            "name": "svgNode1SizePosZ",
            "type": "text",
            "default": "255px,172px,50px,50px,10"
        },
        {
            "name": "svgNode2",
            "type": "text",
            "default": ""
        },
        {
            "name": "svgNode2SizePosZ",
            "type": "text",
            "default": "255px,172px,50px,50px,10"
        },
        {
            "name": "line_1_SizePosZColor",
            "type": "text",
            "default": "145px,4px,7px,283px,0,#ee3041"
        },
        {
            "name": "line_2_SizePosZColor",
            "type": "text",
            "default": "217px,0px,19px,1000px,0,#fff"
        },
        {
            "name": "line_3_SizePosZColor",
            "type": "text",
            "default": "217px,0px,19px,1000px,0,#fff"
        },
        {
            "name": "line_4_SizePosZColor",
            "type": "text",
            "default": "217px,0px,19px,1000px,0,#fff"
        },
        {
            "name": "line_5_SizePosZColor",
            "type": "text",
            "default": "217px,0px,19px,1000px,0,#fff"
        },
        {
            "name": "line_6_SizePosZColor",
            "type": "text",
            "default": "217px,0px,19px,1000px,0,#fff"
        },
        {
            "name": "line_7_SizePosZColor",
            "type": "text",
            "default": "217px,0px,19px,1000px,0,#fff"
        },
        {
            "name": "line_8_SizePosZColor",
            "type": "text",
            "default": "217px,0px,19px,1000px,0,#fff"
        },
        {
            "name": "image_1",
            "type": "image",
            "default": "images/bubble_160600.svg"
        },
        {
            "name": "image_1SizePosZ",
            "type": "text",
            "default": "160px,600px,0px,168px,1"
        },
        {
            "name": "image_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "image_2SizePosZ",
            "type": "text",
            "default": "100%,60px,0,194px"
        },
        {
            "name": "image_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "image_3SizePosZ",
            "type": "text",
            "default": "284px,451px,0px,0px,1"
        },
        {
            "name": "image_4",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "image_4SizePosZ",
            "type": "text",
            "default": "75px,12px,198px,154px,1"
        },
        {
            "name": "image_5",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "image_5SizePosZ",
            "type": "text",
            "default": "19px,12px,72px,100px,1"
        },
        {
            "name": "image_6",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "image_6SizePosZ",
            "type": "text",
            "default": "300px,250px,0px,0px,1"
        },
        {
            "name": "image_7",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "image_7SizePosZ",
            "type": "text",
            "default": "19px,12px,72px,100px,1"
        },
        {
            "name": "image_8",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "image_8SizePosZ",
            "type": "text",
            "default": "300px,250px,0px,0px,1"
        },
        {
            "name": "cta",
            "type": "text",
            "default": "LEARN MORE"
        },
        {
            "name": "ctaTextProps",
            "type": "text",
            "default": "18px,27px,#FFF,center,0px,1px"
        },
        {
            "name": "ctaSizePosZ",
            "type": "text",
            "default": "135px,36px,13px,522px,55"
        },
        {
            "name": "addCtaArrow",
            "type": "text",
            "default": "no"
        },
        {
            "name": "uscLogo",
            "type": "image",
            "default": "images/uscc-logo.svg"
        },
        {
            "name": "uscLogoSizePosZ",
            "type": "text",
            "default": "110px,45px,25px,322px,55"
        },
        {
            "name": "legal",
            "type": "text",
            "default": ""
        },
        {
            "name": "legalTextProps",
            "type": "text",
            "default": "8px,10px,inherit,left,0px,0px"
        },
        {
            "name": "legalSizePosZ",
            "type": "text",
            "default": "auto,auto,134px,228px"
        },
        {
            "name": "legalDetails",
            "type": "text",
            "default": "Via monthly bill credit. New line, an Unlimited Plan, compatible smartphone and credit approval required. Taxes, fees, and restrictions apply."
        },
        {
            "name": "carouselDeviceTotal",
            "type": "text",
            "default": "0"
        },
        {
            "name": "carouselSizePos",
            "type": "text",
            "default": "82px,128px,204px,85px"
        },
        {
            "name": "carouselLogosSizePos",
            "type": "text",
            "default": "60px, 10px, 215px, 220px, 3"
        },
        {
            "name": "carousel_url_1",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_2",
            "type": "text",
            "default": "https://www.uscellular.com/"
        },
        {
            "name": "carousel_img_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_3",
            "type": "text",
            "default": "https://www.uscellular.com/"
        },
        {
            "name": "carousel_img_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "showSnipe",
            "type": "text",
            "default": "no"
        },
        {
            "name": "snipeDate",
            "type": "text",
            "default": "17/08/2020"
        },
        {
            "name": "snipeMaxDays",
            "type": "text",
            "default": "5"
        },
        {
            "name": "snipeText",
            "type": "text",
            "default": "EARLY ACCESS"
        }
    ]
});
