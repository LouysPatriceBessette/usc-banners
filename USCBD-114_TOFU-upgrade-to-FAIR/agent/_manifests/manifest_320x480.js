var myFT = {
 	instantAds : {
    txt_1:"Other carriers",
    txt_1_Props:"27.5px,27.5px,#FFF,left,73px,130px",
    txt_2:"still charge",
    txt_2_Props:"27.5px,27.5px,#FFF,left,73px,165px",
    txt_3:"activation",
    txt_3_Props:"27.5px,27.5px,#FFF,left,73px,200px",
    txt_4:"fees. Really?",
    txt_4_Props:"27.5px,27.5px,#FFF,left,73px,235px",
    txt_5:"We've saved customers",
    txt_5_Props:"25px,normal,#FFFFFF,center,27px,175px",
    txt_6:"ON ACTIVATION FEES",
    txt_6_Props:"27px,15px,#FFF,center,29px,282px",
    txt_7:"this year",
    txt_7_Props:"22px,normal,#FFF,center,117px,314px",
    txt_8:"",
    txt_8_Props:"24.5px,23px,#FFF,left,73px,121px",
    txt_9:"",
    txt_9_Props:"24.5px,23px,#FFF,left,73px,121px",
    txt_10:"",
    txt_10_Props:"24.5px,23px,#FFF,left,73px,121px",
    txt_11:"",
    txt_11_Props:"24.5px,23px,#FFF,left,73px,121px",
    txt_12:"",
    txt_12_Props:"24.5px,23px,#FFF,left,73px,121px",
    svgNode1:"",
    svgNode1SizePosZ:"255px,172px,50px,50px,10",
    svgNode2:"",
    svgNode2SizePosZ:"255px,172px,50px,50px,10",
    line_1_SizePosZColor:"246px,3px,35px,210px,0,#ee3041",
    line_2_SizePosZColor:"217px,0px,19px,1000px,0,#fff",
    line_3_SizePosZColor:"217px,0px,19px,1000px,0,#fff",
    line_4_SizePosZColor:"217px,0px,19px,1000px,0,#fff",
    line_5_SizePosZColor:"217px,0px,19px,1000px,0,#fff",
    line_6_SizePosZColor:"217px,0px,19px,1000px,0,#fff",
    line_7_SizePosZColor:"217px,0px,19px,1000px,0,#fff",
    line_8_SizePosZColor:"217px,0px,19px,1000px,0,#fff",
    image_1:"images/bubble.svg",
    image_1SizePosZ:"187px,200px,57px,0px,1",
    image_2:"images/empty_image.png",
    image_2SizePosZ:"100%,60px,0,194px",
    image_3:"images/empty_image.png",
    image_3SizePosZ:"284px,451px,0px,0px,1",
    image_4:"images/empty_image.png",
    image_4SizePosZ:"75px,12px,198px,154px,1",
    image_5:"images/empty_image.png",
    image_5SizePosZ:"19px,12px,72px,100px,1",
    image_6:"images/empty_image.png",
    image_6SizePosZ:"300px,250px,0px,0px,1",
    image_7:"images/empty_image.png",
    image_7SizePosZ:"19px,12px,72px,100px,1",
    image_8:"images/empty_image.png",
    image_8SizePosZ:"300px,250px,0px,0px,1",
    cta:"LEARN MORE",
    ctaTextProps:"20px,27px,#FFF,center,0px,1px",
    ctaSizePosZ:"150px,39px,85px,414px,55",
    addCtaArrow:"no",
    uscLogo:"images/uscc-logo.svg",
    uscLogoSizePosZ:"152px,45px,74px,363px,55",
    legal:"",
    legalTextProps:"8px,10px,inherit,left,0px,0px",
    legalSizePosZ:"auto,auto,134px,228px",
    legalDetails:"Via monthly bill credit. New line, an Unlimited Plan, compatible smartphone and credit approval required. Taxes, fees, and restrictions apply.",
    carouselDeviceTotal:"0",
    carouselSizePos:"82px,128px,204px,85px",
    carouselLogosSizePos:"60px, 10px, 215px, 220px, 3",
    carousel_url_1:"",
    carousel_img_1:"images/empty_image.png",
    carousel_logo_1:"images/empty_image.png",
    carousel_url_2:"https://www.uscellular.com/",
    carousel_img_2:"images/empty_image.png",
    carousel_logo_2:"images/empty_image.png",
    carousel_url_3:"https://www.uscellular.com/",
    carousel_img_3:"images/empty_image.png",
    carousel_logo_3:"images/empty_image.png",
    showSnipe:"no",
    snipeDate:"17/08/2020",
    snipeMaxDays:"5",
    snipeText:"EARLY ACCESS"
  },
  $: function(str) { return $(str); },
  tracker: function(event, eventDiscription, eventDiscription2) {
    try { console.log(event, " has fired : ",  eventDiscription, " :: ", eventDiscription2)
    }catch(err) { console.log("error in trackings") }
  },
  applyButton: function(thisElement, functionToCall){
    this.$(thisElement).on('click', function(event){
      event.stopPropagation();
      functionToCall();
    })
  },
  applyClickTag: function(thisElement, thisNum, thisURL){
    this.$(thisElement).on('click', function(event){
      event.stopPropagation();
      window.open(window.clickTag,'_blank');
    })
  },
  on: function(thisEventName, thisFunc){
    if(thisEventName == "instantads"){ thisFunc(); }
  }
}