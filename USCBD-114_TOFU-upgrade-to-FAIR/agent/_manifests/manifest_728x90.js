var myFT = {
 	instantAds : {
    txt_1:"Other carriers still charge",
    txt_1_Props:"24.5px,24.5px,#FFF,left,314px,19px",
    txt_2:"still charge",
    txt_2_Props:"24.5px,24.5px,#FFF,left,73px,70px",
    txt_3:"activation",
    txt_3_Props:"24.5px,24.5px,#FFF,left,73px,99px",
    txt_4:"activation fees. Really?",
    txt_4_Props:"24.5px,24.5px,#FFF,left,314px,46px",
    txt_5:"We've<br>saved<br>customers",
    txt_5_Props:"21px,20px,#FFFFFF,right,238px,15px",
    txt_6:"ON ACTIVATION FEES THIS YEAR",
    txt_6_Props:"17px,15px,#FFF,center,388px,71px",
    txt_7:"this year",
    txt_7_Props:"16px,normal,#FFF,center,121px,152px",
    txt_8:"",
    txt_8_Props:"24.5px,23px,#FFF,left,73px,121px",
    txt_9:"",
    txt_9_Props:"24.5px,23px,#FFF,left,73px,121px",
    txt_10:"",
    txt_10_Props:"24.5px,23px,#FFF,left,73px,121px",
    txt_11:"",
    txt_11_Props:"24.5px,23px,#FFF,left,73px,121px",
    txt_12:"",
    txt_12_Props:"24.5px,23px,#FFF,left,73px,121px",
    svgNode1:"",
    svgNode1SizePosZ:"255px,172px,50px,50px,10",
    svgNode2:"",
    svgNode2SizePosZ:"255px,172px,50px,50px,10",
    line_1_SizePosZColor:"296px,4px,361px,58px,0,#ee3041",
    line_2_SizePosZColor:"217px,0px,19px,1000px,0,#fff",
    line_3_SizePosZColor:"217px,0px,19px,1000px,0,#fff",
    line_4_SizePosZColor:"217px,0px,19px,1000px,0,#fff",
    line_5_SizePosZColor:"217px,0px,19px,1000px,0,#fff",
    line_6_SizePosZColor:"217px,0px,19px,1000px,0,#fff",
    line_7_SizePosZColor:"217px,0px,19px,1000px,0,#fff",
    line_8_SizePosZColor:"217px,0px,19px,1000px,0,#fff",
    image_1:"images/bubble_72890.svg",
    image_1SizePosZ:"728px,90px,217px,0px,1",
    image_2:"images/empty_image.png",
    image_2SizePosZ:"100%,60px,0,194px",
    image_3:"images/empty_image.png",
    image_3SizePosZ:"284px,451px,0px,0px,1",
    image_4:"images/empty_image.png",
    image_4SizePosZ:"75px,12px,198px,154px,1",
    image_5:"images/empty_image.png",
    image_5SizePosZ:"19px,12px,72px,100px,1",
    image_6:"images/empty_image.png",
    image_6SizePosZ:"300px,250px,0px,0px,1",
    image_7:"images/empty_image.png",
    image_7SizePosZ:"19px,12px,72px,100px,1",
    image_8:"images/empty_image.png",
    image_8SizePosZ:"300px,250px,0px,0px,1",
    cta:"LEARN MORE",
    ctaTextProps:"14px,27px,#FFF,center,0px,1px",
    ctaSizePosZ:"103px,28px,44px,55px,55",
    addCtaArrow:"no",
    uscLogo:"images/uscc-logo.svg",
    uscLogoSizePosZ:"161px,70px,76px,7px,55",
    legal:"",
    legalTextProps:"8px,10px,inherit,left,0px,0px",
    legalSizePosZ:"auto,auto,134px,228px",
    legalDetails:"Via monthly bill credit. New line, an Unlimited Plan, compatible smartphone and credit approval required. Taxes, fees, and restrictions apply.",
    carouselDeviceTotal:"0",
    carouselSizePos:"82px,128px,204px,85px",
    carouselLogosSizePos:"60px, 10px, 215px, 220px, 3",
    carousel_url_1:"",
    carousel_img_1:"images/empty_image.png",
    carousel_logo_1:"images/empty_image.png",
    carousel_url_2:"https://www.uscellular.com/",
    carousel_img_2:"images/empty_image.png",
    carousel_logo_2:"images/empty_image.png",
    carousel_url_3:"https://www.uscellular.com/",
    carousel_img_3:"images/empty_image.png",
    carousel_logo_3:"images/empty_image.png",
    showSnipe:"no",
    snipeDate:"17/08/2020",
    snipeMaxDays:"5",
    snipeText:"EARLY ACCESS"
  },
  $: function(str) { return $(str); },
  tracker: function(event, eventDiscription, eventDiscription2) {
    try { console.log(event, " has fired : ",  eventDiscription, " :: ", eventDiscription2)
    }catch(err) { console.log("error in trackings") }
  },
  applyButton: function(thisElement, functionToCall){
    this.$(thisElement).on('click', function(event){
      event.stopPropagation();
      functionToCall();
    })
  },
  applyClickTag: function(thisElement, thisNum, thisURL){
    this.$(thisElement).on('click', function(event){
      event.stopPropagation();
      window.open(window.clickTag,'_blank');
    })
  },
  on: function(thisEventName, thisFunc){
    if(thisEventName == "instantads"){ thisFunc(); }
  }
}