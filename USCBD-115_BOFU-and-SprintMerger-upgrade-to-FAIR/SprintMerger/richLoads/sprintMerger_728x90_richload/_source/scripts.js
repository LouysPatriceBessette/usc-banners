


// CURRENT CREATIVE DOM ELEMENT WITH DYNAMIC CONTENT
//Add and remove as you need.
var bgVideo,
    footer,
    frame1Img,
    frame1ImgCropped,
    paperTear,
    paperTear2,
    text_frame2_1,
    text_frame2_2,
    text_frame2_3,
    end_txt_1,
    end_txt_2,
    end_txt_3,
    end_txt_4,
    phoneLogos,
    logo5G,
    grayPhone,
    bluePhone,
    pinkPhone




// STANDARD VARIABLES USUALY NO NEED TO TOUCH
var uscLogo,
    uscLogoWhite,
    container,
    ctaBtn,
    ctaText,
    legalText,
    legalIAText,
    legalDetails,
    legalDetailsText,

    ctaArrow,
    timeline,
    thisCarousel,

    activateCarousel = (myFT.instantAds.carouselDeviceTotal > 0),
    isLegalHidden = true,
    animInited = false,
    adSize,
    isWebkit = navigator.userAgent.indexOf('AppleWebKit') != -1,

    extrudeDuration = 1,
    letterDelay = 0.1


TweenMax.defaultEase = Sine.easeInOut


var timeline = new TimelineMax({paused: true, smoothChildTiming: true, onComplete:onSequenceCompleted})


function initAnimation(){

    if(animInited)
       return

    animInited = true

    var bgSize = adSize.w+"px auto";
    console.log("bgSize", bgSize);
    frame1ImgCropped.style.backgroundSize = bgSize;

    timeline
        .set(ctaBtn, {boxShadow: "0px 2px 8px -2px rgba(0, 0, 0, 0)"})

        .set(image_holder, {width:"-=166", x:"+=166"})
        .set("#image_holder>*", {backgroundPosition:"-166px 0px"})
        .set(paperTear, { backgroundPosition: "0px "+ (adSize.h)+"px", y:-adSize.h, autoAlpha:1})
        .set(reversed_paper, { y: 0, scaleY:-1, transformOrigin:"top left"})
        .set(shadow_gradient, { yPercent: -100, transformOrigin:"top left"})


        .set(crop_reversed_paper_img, { width:166})
        .set(crop_image_holder, {transformOrigin:"166px 0"})
        .set(frame1ImgCropped, {transformOrigin:"top left", width:166, autoAlpha:1})
        .set(crop_reversed_paper, { y: adSize.h*2, scaleY:-1, width:166, transformOrigin:"top left"})


        .set(line_1, {scaleX: 0, autoAlpha: 0, transformOrigin: "50% 50%"})

        .set(["#lockoutHolder>div",legalText], {x:adSize.w, autoAlpha:0})

        // MAKE A ADJUSTMENT FOR THE START
        // POSITION OF THINGS BEFORE THIS LINE
        .set(container, {autoAlpha: 1})

        .add("firstFrame", "+=3.5") // FRAME 1

           

            .to(paperTear, .35, {backgroundPosition: "0px 0px", y:0, ease: Linear.easeNone}, "firstFrame")
            .to(reversed_paper, .35, {y:adSize.h, ease: Linear.easeNone}, "firstFrame")
            .to(image_holder, .35, {y:adSize.h*1.15, x:"-=2", rotation:3,  ease: Linear.easeNone, force3D: false}, "firstFrame")
            .to(frame1Img, .35, {y:-adSize.h, ease: Linear.easeNone}, "firstFrame")
            .to(shadow_gradient, .5, {yPercent:0, ease: Linear.easeNone}, "firstFrame")
            
            .fromTo([line_1], .45,
                {scaleX: 0, autoAlpha: 0, transformOrigin: "50% 50%"},
                {scaleX: 1, autoAlpha: 1, ease: Sine.easeIn}, "firstFrame+=1")


        .add("secondFrame", "+=3") // FRAME 2
            .to(text_frame2_2, .5, {autoAlpha:0}, "secondFrame")
            .to(text_frame2_1, .5, {y:5}, "secondFrame+=.5")
            .to(line_1, .5, {y:8}, "secondFrame+=.5")
            .to(text_frame2_3, .5, {autoAlpha:1}, "secondFrame+=1")


        .add("thirdFrame", "+=3") // FRAME 3

            .to(paperTear, .25, {  backgroundPosition: "0px "+ (adSize.h)+"px", y:-adSize.h, ease: Linear.easeNone}, "thirdFrame")
            .to(frame1ImgCropped, .2, {height:0, y:-20, ease: Linear.easeNone}, "thirdFrame")
            .to(crop_image_holder, .25, {y:-adSize.h, rotation:10, x:10, ease: Linear.easeNone}, "thirdFrame")
            .to(crop_reversed_paper, .25, {y:adSize.h, x:0, ease: Linear.easeNone}, "thirdFrame")
            // .to(crop_reversed_paper_img, .45, {height:85, ease: Linear.easeNone}, "thirdFrame")

            .to([line_1, text_frame2_1, text_frame2_3], .5, {autoAlpha: 0}, "thirdFrame")
            .to(bg, .75, {autoAlpha: 1, ease: Sine.easeIn}, "thirdFrame+=.25")


            .fromTo([line_2], .45,
                {scaleX: 0, autoAlpha: 0, transformOrigin: "0% 50%"},
                {scaleX: 1, autoAlpha: 1, ease: Sine.easeIn}, "thirdFrame+=2.5")

            .to(["#lockoutHolder>div",legalText], .5, {x: 0, autoAlpha:1, ease:Circ.easeOut}, "thirdFrame+=2")

            .to(deviceHolder, .75, { autoAlpha: 1}, "thirdFrame+=1")
            .to(deviceHolder, 2, { rotationY:30, yoyo:true, repeat:1, repeatDelay:.6, ease: Quart.easeInOut }, "thirdFrame+=1")
            .to(pinkPhone, 2, {x:-5, yoyo:true, repeat:1, repeatDelay:.55, ease: Quart.easeInOut }, "thirdFrame+=1")
            .to(grayPhone, 2, {x:5, yoyo:true, repeat:1, repeatDelay:.55, ease: Quart.easeInOut }, "thirdFrame+=1")

            .to(phoneLogos, .5, {autoAlpha:1}, "thirdFrame+=3.5")

            

        .add("endFrame", "+=0.25") // FRAME 4

        // Reveal snipe if needed
        .add( addSnipeIfNeeded(), "endFrame+=0.1")

        //auto play carousel at end
        .call(function () {
            if (thisCarousel != undefined && thisCarousel.deviceTotal() > 1)
                thisCarousel.play();
        }, this, "endFrame+=1")


        .call(onOverCTA, [], this, "endFrame+=0.8")

        // .timeScale(.35) // FOR DEBUGGING ANIMATION
        .play();

        // END OF the main timeline initialization

        function addSnipeIfNeeded() {

            if (myFT.instantAds.showSnipe != "yes")
                return;

            var snipeTL = new TimelineMax();

            if (snipe.style.display == "none") {
                snipe.style.display = "flex";
                snipeTL.to(snipe, 0.35, {autoAlpha: 1})
            }
            return snipeTL;
        }


        /*
          EXAMPLE OF SUB TIMELINE
          Define as a function that return a new timeline so it won't play to soon.
          use : .add(sevenHundredsTL(), "secondFrame+=1")
        */
        // function sevenHundredsTL() {

        //     return [
        //         new TimelineMax({ delay: 0 * letterDelay }).to("#sevenHundreds-shadow-t1-p", extrudeDuration, { morphSVG: "M40.27889,59.9414c4.44948-2.23865,7.23626-6.42474,7.23626-12.76768,0-8.70563-5.29315-11.56155-14.06831-13.99908l.00395-7.38936,11.76866-6.19967-.00246-4.727a21.27547,21.27547,0,0,0-11.77015-4.24837V6.36184H28.43242v.06289H28.3115v.06293L16.22316,12.71557v2.89756c-.02448.01362-.05048.02577-.07485.03955-.03958.02186-.08209.041-.12137.06319L9.62012,19.05a12.28563,12.28563,0,0,0-6.49,11.21587c0,8.07894,5.01442,11.213,13.02352,13.58068v8.13238h-1.7895v.06289l-3.143,1.57272v.06289h-.01457l.01457-.06289L2.15485,58.33266h0v4.80522a23.7814,23.7814,0,0,0,13.99877,5.08456v6.89479h4.93829l12.28787-6.36208-.00247-5.22164.01139-.00645Z" }),
        //         new TimelineMax({ delay: 0 * letterDelay }).to("#sevenHundreds-extrusion-t1-p", extrudeDuration, { morphSVG: "M22.26148,62.59131l6.10388,6.16384h5.01442V61.79053c8.28783-.76636,14.13784-5.43251,14.13784-14.62516,0-3.84344-1.03484-6.54433-2.94243-8.5748-.02007-.02171-.04281-.04148-.06307-.06308l-6.22973-6.22768a14.76558,14.76558,0,0,0-4.83308-3.2837V15.616a14.70463,14.70463,0,0,1,8.6361,3.96963h3.134V15.03969l.00247.00239-.00247-.01974v-.17211l-.02356-.01651.02356.01651-6.35558-6.3535.01424.02877-.01424-.00994V8.49673a21.87709,21.87709,0,0,0-6.5091-3.31752L27.09373,0H22.07931V4.31759c-8.28753.97526-13.09306,6.129-13.09306,13.23272,0,4.04445,1.25714,6.84932,3.51712,8.93946.02167.02035.04088.04251.06274.0627.02182.02046.04114.04278.06311.06312l6.10388,6.10186c.02167.02035.04087.04251.06274.0627a14.83647,14.83647,0,0,0,3.21394,2.24127V50.49215a16.39321,16.39321,0,0,1-10.86473-4.87506H8.011v4.80522l6.29266,6.27509v.01548c.02064.016.0422.03143.06292.04741v.01552a23.241,23.241,0,0,0,7.6432,4.00728v1.61857h.06292v.0474l-.06292-.0474.2517.25163m-.18217-37.23293c-.37574-.11481-.73266-.23163-1.08024-.35a11.24427,11.24427,0,0,1-.08566-1.37548,7.8418,7.8418,0,0,1,1.1659-4.30271Zm.05632,37.07614-.04239-.03195h.04239Zm.06293.04741-.02179-.01644h.02179Zm.06292.0474-.00125-.001h.00125ZM27.09373.06289V.04771l.00379.01518Zm6.286,47.42721V38.87754c.72931.21087,1.40921.41961,2.04167.634A12.44524,12.44524,0,0,1,35.52093,41.09,9.07066,9.07066,0,0,1,33.37978,47.4901Z" }),
        //         new TimelineMax({ delay: 0 * letterDelay }).to("#sevenHundreds-letter-t1-p", extrudeDuration, { morphSVG: "M27.0242,62.40166H22.00978V55.50687A23.78136,23.78136,0,0,1,8.011,50.42231V45.61709h3.13405a16.39321,16.39321,0,0,0,10.86473,4.87506V31.131C14.00067,28.7633,8.98625,25.62925,8.98625,17.55031c0-7.10368,4.80553-12.25746,13.09306-13.23272V0h5.01442V4.24837A21.27543,21.27543,0,0,1,38.86387,8.49673v4.73539H35.72982a14.70452,14.70452,0,0,0-8.63609-3.96964V26.8128c8.77515,2.43752,14.0683,5.29345,14.0683,13.99908,0,9.19264-5.85,13.8588-14.13783,14.62516ZM22.07931,9.40215c-5.36268.90481-7.52148,4.03886-7.52148,7.93927,0,4.80522,2.50706,6.54683,7.52148,8.07894ZM27.0242,50.35309c5.36268-.69653,8.49673-3.55185,8.49673-9.26309,0-5.36268-2.71626-6.89479-8.49673-8.566Z" }),
                    // etc...
        //     ]
        // }

}





/*------------------------------------------------------------------------*/


window.onload = function(){
    fillDynamicContent();
    TweenMax.delayedCall(.4, initAnimation);
}



//WHEN SOMETHING MUST BE DONE AFTER THE ANIMATION.
function onSequenceCompleted() {
    // Good place to start the rollover listener so it does not
    // take place in the middle of the animation
    container.onmouseenter = onOverCTA
}

// DEFINE ROLL OVER EFFECTS
function onOverCTA() {

    TweenMax.set(ctaBtn, { transformOrigin: "center bottom" })
    TweenMax.set(cta_gradient, { x: -30, force3D: false })

    TweenMax.to(ctaBtn, 0.2, { scale: 1.08, boxShadow: "6px 16px 10px -8px rgba(0,0,0,0.2)", force3D: false, })
    TweenMax.to(ctaBtn, 0.2, { scale: 1.0, boxShadow: "0px 2px 8px -2px rgba(0, 0, 0, 0)", force3D: false, delay: .4 })
    TweenMax.to(cta_gradient, 0.6, { x: "+=300", ease: Power0.easeNone, force3D: false, delay: .15 })
}


/*------------------------------------------------------------------------*/

function fillDynamicContent(){


    // CURRENT CREATIVE DOM ELEMENT WITH DYNAMIC CONTENT
    //Add and remove as you need.
    bgVideo = getAssets("#bgVideo")
    footer = getAssets("#footer")
    frame1Img = getAssets("#frame1Img")
    frame1ImgCropped = getAssets("#crop_frame1Img")
    paperTear = getAssets("#paperTear")
    paperTear2 = getAssets("#paperTear2")
    text_frame2_1 = getAssets("#text_frame2_1")
    text_frame2_2 = getAssets("#text_frame2_2")
    text_frame2_3 = getAssets("#text_frame2_3")
    end_txt_1 = getAssets("#end_txt_1")
    end_txt_2 = getAssets("#end_txt_2")
    end_txt_3 = getAssets("#end_txt_3")
    end_txt_4 = getAssets("#end_txt_4")
    phoneLogos = getAssets("#phoneLogos")
    logo5G = getAssets("#logo5G")
    grayPhone = getAssets("#grayPhone")
    bluePhone = getAssets("#bluePhone")
    pinkPhone = getAssets("#pinkPhone")


    // STANDARD VARIABLES USUALY NO NEED TO TOUCH
    uscLogo = getAssets("#uscLogo")
    uscLogoWhite = getAssets("#uscLogoWhite")
    container = getAssets("#container")
    ctaBtn = getAssets("#cta_btn")
    ctaText = getAssets("#cta_text")
    legalText = getAssets("#legal_text")
    legalIAText = getAssets("#legal_ia_text")
    legalDetails = getAssets("#legal_details")
    legalDetailsText = getAssets("#legal_details_text")

    adSize = {w: container.offsetWidth, h: container.offsetHeight}


    //TEXT FIELDS
    var textFields = [text_frame2_1, text_frame2_2, text_frame2_3, end_txt_1, end_txt_2, end_txt_3, end_txt_4]

    //IMAGE AS DIV BACKGROUND
    var imageFields = [frame1Img, frame1ImgCropped, paperTear, paperTear2, uscLogo, grayPhone, bluePhone, pinkPhone, logo5G, phoneLogos]

    var svgNodes = []



    //LINES
    var lines = document.querySelectorAll(".line")

    // DYNAMIC IA SETUP
    // populate instantAd components
    myFT.applyClickTag(container, 1, myFT.instantAds.clickTagURL)

    myFT.applyButton(container, function () {
        myFT.tracker("bannerClick", "banner container clicked")
    })


    // CTA BUTTON
    var ctaBtnProps = splitValues(myFT.instantAds.ctaSizePosZ)
    ctaBtn.style.width = ctaBtnProps[0]
    ctaBtn.style.height = ctaBtnProps[1]
    ctaBtn.style.left = ctaBtnProps[2]
    ctaBtn.style.top = ctaBtnProps[3]
    ctaBtn.style.zIndex = ctaBtnProps[4]

    // CTA BUTTON TEXT
    var ctaTxtProps = splitValues(myFT.instantAds.ctaTextProps)
    ctaText.innerHTML = myFT.instantAds.cta
    ctaText.style.fontSize = ctaTxtProps[0]
    ctaText.style.lineHeight = ctaTxtProps[1]
    ctaText.style.color = ctaTxtProps[2]
    ctaText.style.textAlign = ctaTxtProps[3]
    ctaText.style.left = ctaTxtProps[4]
    ctaText.style.paddingTop = ctaTxtProps[5]


    if(myFT.instantAds.addCtaArrow == 'yes'){
        ctaText.innerHTML = ctaText.innerHTML + '<svg id="ctaArrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.99 20.7"><polygon class="cls-1" fill="' + ctaTxtProps[2] +' " points="3.45 20.7 0 17.24 7.1 10.19 0.31 3.43 3.73 0 13.99 10.21 3.45 20.7"/></svg>'
        ctaArrow = getAssets("#ctaArrow")
        var arrowW = parseInt(ctaTxtProps[0].replace("px","") - +2)
        var arrowH = Math.ceil(parseFloat(ctaTxtProps[0].replace("px","")) * .75)
        ctaArrow.style.width =  arrowW + "px"
        ctaArrow.style.height =  arrowH + "px"
        ctaArrow.style.left =  (arrowW * .15) + "px"
    }



    //TEXTS
    // Set all textField properties with manifest value
    for (var i = 0; i < textFields.length; ++i) {

        var assetName = (textFields[i].getAttribute("data-ia") || textFields[i].id).toString()
        var props = splitValues(myFT.instantAds[assetName + '_Props'])
        textFields[i].innerHTML = myFT.instantAds[assetName]
        textFields[i].style.fontSize = props[0]
        textFields[i].style.lineHeight = props[1]
        textFields[i].style.color = props[2]
        textFields[i].style.textAlign = props[3]
        textFields[i].style.left = props[4]
        textFields[i].style.top = props[5]
    }


    /*------------------------------------------------------------------------*/

    //IMAGE AS DIV BACKGROUND
    // Set all properties of all image (use div with background image)
    for (var i = 0; i < imageFields.length; ++i) {

        var id = imageFields[i].id
        var assetName = (imageFields[i].getAttribute("data-ia") || id).toString()
        var props = splitValues(myFT.instantAds[assetName + 'SizePosZ'])

        imageFields[i].style.backgroundImage = "url(" + myFT.instantAds[assetName] + ")"
        imageFields[i].style.width = props[0]
        imageFields[i].style.height = props[1]
        imageFields[i].style.left = props[2]
        imageFields[i].style.top = props[3]
        imageFields[i].style.zIndex = props[4]

        if (assetName == 'uscLogo' && uscLogoWhite != undefined) {
            //match logo with white logo
            uscLogoWhite.style.backgroundImage = "url(" + myFT.instantAds[assetName] + ")";
            uscLogoWhite.style.width = props[0];
            uscLogoWhite.style.height = props[1];
            uscLogoWhite.style.left = props[2];
            uscLogoWhite.style.top = props[3];
            uscLogoWhite.style.zIndex = props[4];
        }

        if(id == 'pinkPhone'){
            var holder = getAssets("#deviceHolder")
            holder.style.width = props[0]
            holder.style.height = props[1]
            holder.style.transformOrigin = "50% 50%"
        }


        if(id == 'frame1Img'){

            var holder = getAssets("#image_holder")
            holder.style.position = "absolute";
            holder.style.width = props[0];
            holder.style.height = props[1];
            holder.style.left = "0px";
            holder.style.top = "0px";
            holder.style.zIndex = props[4]

            var reversed = getAssets("#reversed_paper")
            reversed.style.backgroundRepeat = "no-repeat";
            reversed.style.backgroundImage = "url(" + myFT.instantAds[assetName] + ")";
            reversed.style.zIndex = props[4]

            var cropHolder = getAssets("#crop_image_holder")
            cropHolder.style.position = "absolute";
            cropHolder.style.width = props[0];
            cropHolder.style.height = props[1];
            cropHolder.style.left = "0px";
            cropHolder.style.top = "0px";
            cropHolder.style.zIndex = props[4]

            var cropReversedImg = getAssets("#crop_reversed_paper_img")
            cropReversedImg.style.backgroundRepeat = "no-repeat";
            cropReversedImg.style.backgroundImage = "url(" + myFT.instantAds[assetName] + ")";
            cropReversedImg.style.zIndex = props[4]
        }


    }
    /*------------------------------------------------------------------------*/

    //INSERT SVG NODE SO IT CAN BE MANIPULATED
    // Set all properties of all image (use div with background image)
    for (var i = 0; i < svgNodes.length; ++i) {

        var assetName = (svgNodes[i].getAttribute("data-ia") || svgNodes[i].id).toString()
        var props = splitValues(myFT.instantAds[assetName + 'SizePosZ'])

        svgNodes[i].innerHTML = myFT.instantAds[assetName]

        svgNodes[i].style.width = props[0]
        svgNodes[i].style.height = props[1]
        svgNodes[i].style.left = props[2]
        svgNodes[i].style.top = props[3]
        svgNodes[i].style.zIndex = props[4]

    }

    /*------------------------------------------------------------------------*/


    if(bgVideo != undefined)
    {
        bgVideo.pause();
        bgVideo.currentTime = 0;

        //VIDEOS
        var props = splitValues(myFT.instantAds.bgVideoSizePosZColor)

        bgVideo.style.width = props[0]
        bgVideo.style.height = props[1]
        bgVideo.style.left = props[2]
        bgVideo.style.top = props[3]
        bgVideo.style.zIndex = props[4]
        bgVideo.style.backgroundColor = props[5]
    }

    /*------------------------------------------------------------------------*/

    // LEGAL TEXT & BUTTON
    var legalTxtProps = splitValues(myFT.instantAds.legalTextProps)
    legalIAText.innerHTML = myFT.instantAds.legal
    legalIAText.style.fontSize = legalTxtProps[0]
    legalIAText.style.lineHeight = legalTxtProps[1]
    legalIAText.style.color = legalTxtProps[2]
    legalIAText.style.left = legalTxtProps[4]
    legalIAText.style.top = legalTxtProps[5]
    legalText.style.textAlign = legalTxtProps[3]

    var legalTextSizeProps = splitValues(myFT.instantAds.legalSizePosZ)
    legalText.style.width = legalTextSizeProps[0]
    legalText.style.height = legalTextSizeProps[1]
    legalText.style.left = legalTextSizeProps[2]
    legalText.style.top = legalTextSizeProps[3]
    legalText.style.zIndex = legalTextSizeProps[4]

    // // check to see if there are legal details that would display on click
    if (myFT.instantAds.legalDetails != "") {
        var legalDetailProps = splitValues(myFT.instantAds.legalDetailsZ)
        legalDetails.style.display = "inline"
        legalDetailsText.innerHTML = myFT.instantAds.legalDetails
        legalDetails.style.zIndex = legalDetailProps[0]
    }
    /*------------------------------------------------------------------------*/

    //SET LINES PROPS

    if(lines != null && lines != undefined) {
        // Set all properties of all lines
        for (var i = 0; i < lines.length; i++) {

            var assetName = lines[i].id.toString()
            var props = splitValues(myFT.instantAds[assetName + '_SizePosZColor'])
            lines[i].style.width = props[0]
            lines[i].style.height = props[1]
            lines[i].style.left = props[2]
            lines[i].style.top = props[3]
            lines[i].style.zIndex = props[4]
            lines[i].style.backgroundColor = props[5]
        }
    }
}


/*----- UTILS METHOD -------------------------------------------------------------------*/

function splitValues(values) {
    return values.split(',')
}

function getAssets(cssPointer) {
    // Return undefined, or a direct reference
    // if only one match or the array of matches.
    var a = myFT.$(cssPointer)
    var returnVal = undefined
    if (a != undefined && a.length > 0)
        returnVal = a.length == 1 ? a[0] : a
    return returnVal
}

function showLegal() {
    TweenMax.to(legalDetails, 0.10, {autoAlpha: 1})
    myFT.tracker("legalClick", "show legal")
    isLegalHidden = false
}

function hideLegal() {
    TweenMax.to(legalDetails, 0.10, {autoAlpha: 0})
    myFT.tracker("legalClick", "hide legal")
    isLegalHidden = true
}


var Carousel = {}

if(activateCarousel){
    // Create the carousel object
    Carousel.init = function() {

    var deviceCarousel = getAssets("#device_carousel"),
    devicesContainer = getAssets("#device_sets_container"),
    carousel_logo_holder = getAssets("#carousel_logo_holder"),
    carouselControls = getAssets(".carousel-controls"),
    carouselLeft = getAssets("#carousel_left"),
    carouselRight = getAssets("#carousel_right"),
    carouselDeviceImages = [],
    carouselDeviceLogos = [],
    carouselDeviceLinks = [],
    devices = [],
    logos = [],
    carouselLogosProps,
    carouselPaused = true,
    deviceTotal = null,
    currentDeviceNum = 0,
    carouselOffset,
    logosOffset,
    autoPlayCancelled = false,
    onCreated = function(){

        deviceTotal = myFT.instantAds.carouselDeviceTotal
        var carouselProps = splitValues(myFT.instantAds.carouselSizePos)
        deviceCarousel.style.width = carouselProps[0]
        deviceCarousel.style.height = carouselProps[1]
        deviceCarousel.style.left = carouselProps[2]
        deviceCarousel.style.top = carouselProps[3]
        deviceCarousel.style.cursor = "pointer"


        // Remove the arrow if showing only one device or
        // activateCarouselArrow = 'no'
        if(deviceTotal > 1 && (activateCarouselArrow == "yes")){
            carouselLeft.addEventListener("click", onLeftClick)
            carouselRight.addEventListener("click", onRightClick)
        }else{
            carouselControls[0].style.display = "none"
            carouselControls[1].style.display = "none"
        }

        carouselOffset = devicesContainer.offsetWidth

        carouselLogosProps = splitValues(myFT.instantAds.carouselLogosSizePos)
        carousel_logo_holder.style.width = carouselLogosProps[0]
        carousel_logo_holder.style.height = carouselLogosProps[1]
        carousel_logo_holder.style.left = carouselLogosProps[2]
        carousel_logo_holder.style.top = carouselLogosProps[3]

        logosOffset = parseInt(carouselLogosProps[0].replace('px',''))


        //Retrieve carousel config from InstantAds data
        for (var i=1; i<=deviceTotal; i++) {

            var imgSearch =/\.(?:jpg|gif|png|svg)/gi
            var deviceSrcImg = myFT.instantAds["carousel_img_" + i]
            var deviceLogoSrcImg = myFT.instantAds["carousel_logo_" + i]
            var deviceClickTag = myFT.instantAds["carousel_url_" + i]
            carouselDeviceImages.push(deviceSrcImg)
            carouselDeviceLinks.push(deviceClickTag)

            var imgExist = imgSearch.test(deviceLogoSrcImg)
            carouselDeviceLogos.push( imgExist ? deviceLogoSrcImg : 'none')
        }


        //Create new HTML for the carousel
        for(var i=0; i<deviceTotal; i++) {

            var device = document.createElement("div")
            device.id = "device" + (i+1)
            device.className = "device"
            device.style.backgroundImage = 'url('+carouselDeviceImages[i]+')'

            if(deviceTotal>1)
                myFT.applyClickTag( device, (2+i), carouselDeviceLinks[i] )
            devices.push(device)

            devicesContainer.appendChild(device)

            TweenMax.set(device, {x:carouselOffset})

            if(carouselDeviceLogos[i-1] != 'none'){
            var logoImg = document.createElement("div")
                logoImg.id = "device_logo" + (i + 1)
                logoImg.className = "devicelogo"
                logoImg.style.backgroundImage = 'url('+carouselDeviceLogos[i];+')'
                carousel_logo_holder.appendChild(logoImg)
                logos.push(logoImg)
                TweenMax.set(logoImg, {x:logosOffset})
            }

        }

        function onLeftClick(evt) {
            myFT.tracker("leftCarouselClick", "clicking on carousel left arrow")
            evt.stopPropagation()
            carouselPaused = true
            clearAutoPlay()
            goLeft()
        }

        function onRightClick(evt) {
            myFT.tracker("rightCarouselClick", "clicking on carousel right arrow")
            evt.stopPropagation()
            carouselPaused = true
            clearAutoPlay()
            goRight()
        }

        TweenMax.to([devices[0], logos[0]], 0.3, {x:0})

    },

        goLeft = function() {
        TweenMax.killAll()
        var dOut = devices[currentDeviceNum]
        var lOut = logos[currentDeviceNum]
        if ( currentDeviceNum == 0)
            currentDeviceNum = deviceTotal
        currentDeviceNum--
        var dIn = devices[currentDeviceNum]
        var lIn = logos[currentDeviceNum]

        TweenMax.to(dOut, 0.65, {x:"+="+carouselOffset, alpha:0, ease:Power1.easeInOut})
        TweenMax.to(lOut, 0.65, {x:"+="+logosOffset, alpha:0, ease:Power1.easeInOut})
        TweenMax.fromTo(dIn, 0.65, {x:-carouselOffset}, {x:0, alpha:1, ease:Power1.easeInOut})
        TweenMax.fromTo(lIn, 0.65, {x:-logosOffset}, {x:0, alpha:1, ease:Power1.easeInOut})
    },
    goRight = function(stopAutoplay) {
        TweenMax.killAll()
        var dOut = devices[currentDeviceNum]
        var lOut = logos[currentDeviceNum]
        if (++currentDeviceNum == deviceTotal) {
            currentDeviceNum = 0
            clearAutoPlay()
        }
        var dIn = devices[currentDeviceNum]
        var lIn = logos[currentDeviceNum]
        TweenMax.to(dOut, 0.65, {x:"-="+carouselOffset, alpha:0, ease:Power1.easeInOut})
        TweenMax.to(lOut, 0.65, {x:"-="+logosOffset, alpha:0, ease:Power1.easeInOut})
        TweenMax.fromTo(dIn, 0.65, {x:carouselOffset}, {x:0, alpha:1, ease:Power1.easeInOut})
        TweenMax.fromTo(lIn, 0.65, {x:logosOffset}, {x:0, alpha:1, ease:Power1.easeInOut})
        },
        clearAutoPlay = function() {
            autoPlayCancelled = true
        },
        playCarousel = function() {
        if (carouselPaused || autoPlayCancelled)
            return

        goRight()
        TweenMax.delayedCall( 2.5, playCarousel )
        },
        startCarousel = function(){
        carouselPaused = false
        playCarousel()
        },
        splitValues = function(values) {
        return values.split(',')
        }

        return {
            deviceCarousel: deviceCarousel,
            logos:carousel_logo_holder,
            devicesContainer: devicesContainer,
            controls : {left:carouselLeft, right:carouselRight},
            carouselDeviceImages : carouselDeviceImages,
            carouselDeviceLogos : carouselDeviceLogos,
            carouselDeviceLinks : carouselDeviceLinks,
            carouselPaused : carouselPaused,
            devices : devices,
            deviceTotal : function(){return deviceTotal},
            currentDeviceNum : currentDeviceNum,
            carouselOffset : carouselOffset,
            create : onCreated,
            goLeft : goLeft,
            goRight : goRight,
            clearAutoPlay : clearAutoPlay,
            play : startCarousel
        }
    }
    thisCarousel = Carousel.init(this)
    thisCarousel.create()
}
