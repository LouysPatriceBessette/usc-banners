FT.manifest({
    "filename": "index.html",
    "width": 320,
    "height": 50,
    "clickTagCount": 1,
    "hideBrowsers": ["ie10", "opera"],
    "trackingEvents": [
        {"name": "bannerClick", "type": "string"},
        {"name": "legalClick", "type": "string"}
    ],
    "videos": [
        {"name": "bgVideo", "ref": "127737/empty_video"}
    ],
    "richLoads": [
        {"name": "richload", "src": "sprintMerger_320x50_richload"}
        
    ],
    "instantAds":[
        {
            "name":"creative",
            "type":"text",
            "default":""
        },
        {
            "name":"clickTagURL",
            "type":"text",
            "default":"https://www.uscellular.com/savingsinvitation?utm_campaign=[%campaignName%]&utm_source=ttd&utm_medium=ban&utm_content=[%FT_CREATIVE_ID%]^[%FT_CONFIGURATION_ID%]&utm_tactic=[%FT_PLACEMENT_ID%]"
        },
        {
            "name":"richload",
            "type":"richload",
            "default":"sprintMerger_320x50_richload/index.html"
        },
        {
            "name":"bgVideo",
            "type":"video",
            "default":"127737/empty_video"
        },
        {
            "name":"bgVideoSizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,unset,#fff"
        },
        {
            "name":"txt_1",
            "type":"text",
            "default":"Sprint Customers,"
        },
        {
            "name":"txt_1_Props",
            "type":"text",
            "default":"11px,normal,#fff,center,89px,4px"
        },
        {
            "name":"txt_2",
            "type":"text",
            "default":"WHY RISK A SERVICE<br>DOWNGRADE?"
        },
        {
            "name":"txt_2_Props",
            "type":"text",
            "default":"14px,14px,#fff,center,62px,20px"
        },
        {
            "name":"txt_3",
            "type":"text",
            "default":"Switch from Sprint"
        },
        {
            "name":"txt_3_Props",
            "type":"text",
            "default":"9.5px,normal,#FFFFFF,center,51px,1px"
        },
        {
            "name":"txt_4",
            "type":"text",
            "default":"<sup>$</sup>700 OFF"
        },
        {
            "name":"txt_4_Props",
            "type":"text",
            "default":"23px,normal,#FFFFFF,center,42px,12px"
        },
        {
            "name":"txt_5",
            "type":"text",
            "default":"latest<br>phones"
        },
        {
            "name":"txt_5_Props",
            "type":"text",
            "default":"10px,9px,#fff,left,145px,14px"
        },
        {
            "name":"txt_6",
            "type":"text",
            "default":"No Activation Fee"
        },
        {
            "name":"txt_6_Props",
            "type":"text",
            "default":"9px,normal,#fff,center,52px,38px"
        },
        {
            "name":"txt_7",
            "type":"text",
            "default":"UPGRADE<br>TO FAIR"
        },
        {
            "name":"txt_7_Props",
            "type":"text",
            "default":"16px,14px,#fff,center,95px,20px"
        },
        {
            "name":"txt_8",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_8_Props",
            "type":"text",
            "default":"0px,normal,#fff,center,0px,0px"
        },
        {
            "name":"txt_9",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_9_Props",
            "type":"text",
            "default":"0px,normal,#fff,center,0px,0px"
        },
        {
            "name":"txt_10",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_10_Props",
            "type":"text",
            "default":"0px,normal,#fff,center,0px,0px"
        },
        {
            "name":"txt_11",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_11_Props",
            "type":"text",
            "default":"0px,normal,#fff,center,0px,0px"
        },
        {
            "name":"txt_12",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_12_Props",
            "type":"text",
            "default":"0px,normal,#fff,center,0px,0px"
        },
        {
            "name":"svgNode1",
            "type":"text",
            "default":"<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 255 172'><path d='M92,172.1V149.9H18.9c-9,0-16.4-5.6-16.4-12.6V15.3C2.5,8.4,9.9,2.7,18.9,2.7H237.2c9,0,16.4,5.6,16.4,12.6v122c0,6.9-7.4,12.6-16.4,12.6H119.9L92,171' style='fill: none;stroke: #fff;stroke-miterlimit: 10;stroke-width: 3px'/></svg>"
        },
        {
            "name":"svgNode1SizePosZ",
            "type":"text",
            "default":"255px,172px,50px,50px,10"
        },
        {
            "name":"svgNode2",
            "type":"text",
            "default":""
        },
        {
            "name":"svgNode2SizePosZ",
            "type":"text",
            "default":""
        },
        {
            "name":"line_1_SizePosZColor",
            "type":"text",
            "default":"89px,2px,88px,16px,unset,#ee3040"
        },
        {
            "name":"line_2_SizePosZColor",
            "type":"text",
            "default":"92px,2px,49px,33px,unset,#ee3040"
        },
        {
            "name":"line_3_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_4_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_5_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_6_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_7_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_8_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"image_1",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_1SizePosZ",
            "type":"text",
            "default":"50px,140px,0px,0px,1"
        },
        {
            "name":"image_2",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_2SizePosZ",
            "type":"text",
            "default":"50px,140px,0px,0px,1"
        },
        {
            "name":"image_3",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_3SizePosZ",
            "type":"text",
            "default":"50px,140px,0px,0px,1"
        },
        {
            "name":"image_4",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_4SizePosZ",
            "type":"text",
            "default":"130px,20px,386px,66px,1"
        },
        {
            "name":"image_5",
            "type":"image",
            "default":"images/logo_5G.svg"
        },
        {
            "name":"image_5SizePosZ",
            "type":"text",
            "default":"14px,9px,171px,14px,unset"
        },
        {
            "name":"image_6",
            "type":"image",
            "default":"images/sprintMerger_320x50_f1.jpg"
        },
        {
            "name":"image_6SizePosZ",
            "type":"text",
            "default":"320px,50px,0px,0px,unset"
        },
        {
            "name":"image_7",
            "type":"image",
            "default":"images/tear_32050.png"
        },
        {
            "name":"image_7SizePosZ",
            "type":"text",
            "default":"20px,50px,30px,0px,unset"
        },
        {
            "name":"image_8",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_8SizePosZ",
            "type":"text",
            "default":"320px,50px,0px,0px,1"
        },
        {
            "name": "cta",
            "type": "text",
            "default": "SWITCH NOW"
        },
        {
            "name": "ctaTextProps",
            "type": "text",
            "default": "8px,1em,#FFF,center,0px,1px"
        },
        {
            "name": "ctaSizePosZ",
            "type": "text",
            "default": "58px,16px,246px,29px,unset"
        },
        {
            "name":"addCtaArrow",
            "type":"text",
            "default":"no"
        },
        {
            "name": "uscLogo",
            "type": "image",
            "default": "images/uscc-logo.svg"
        },
        {
            "name": "uscLogoSizePosZ",
            "type": "text",
            "default": "85px,21px,230px,4px,unset"
        },
        {
            "name":"legal",
            "type":"text",
            "default":"Terms Apply"
        },
        {
            "name":"legalTextProps",
            "type":"text",
            "default":"7px,7px,#fff,left,0px,0px"
        },
        {
            "name":"legalSizePosZ",
            "type":"text",
            "default":"auto,auto,142px,35px,50"
        },
        {
            "name":"legalDetails",
            "type":"text",
            "default":""
        },
        {
            "name":"legalDetailsZ",
            "type":"text",
            "default":"0"
        },
        {
            "name": "carouselDeviceTotal",
            "type": "text",
            "default": "0"
        },
        {
            "name": "carouselSizePos",
            "type": "text",
            "default": "78px,131px,185px,56px"
        },
        {
            "name": "carouselLogosSizePos",
            "type": "text",
            "default": "76px, 11px, 186px, 188px"
        },
        {
            "name": "carousel_url_1",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_2",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_3",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "showSnipe",
            "type": "text",
            "default": "yes"
        },
        {
            "name": "snipeDate",
            "type": "text",
            "default": "17/08/2020"
        },
        {
            "name": "snipeMaxDays",
            "type": "text",
            "default": "5"
        },
        {
            "name": "snipeText",
            "type": "text",
            "default": "EARLY ACCESS"
        }
        
    ]
});
