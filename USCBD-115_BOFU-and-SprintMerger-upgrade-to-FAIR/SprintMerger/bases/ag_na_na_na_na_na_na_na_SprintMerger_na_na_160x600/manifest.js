FT.manifest({
    "filename": "index.html",
    "width": 160,
    "height": 600,
    "clickTagCount": 1,
    "hideBrowsers": ["ie10", "opera"],
    "trackingEvents": [
        {"name": "bannerClick", "type": "string"},
        {"name": "legalClick", "type": "string"}
    ],
    "videos": [
        {"name": "bgVideo", "ref": "127737/empty_video"}
    ],
    "richLoads": [
        {"name": "richload", "src": "sprintMerger_160x600_richload"}
    ],
    "instantAds":[
        {
            "name":"creative",
            "type":"text",
            "default":""
        },
        {
            "name":"clickTagURL",
            "type":"text",
            "default":"https://www.uscellular.com/savingsinvitation?utm_campaign=[%campaignName%]&utm_source=ttd&utm_medium=ban&utm_content=[%FT_CREATIVE_ID%]^[%FT_CONFIGURATION_ID%]&utm_tactic=[%FT_PLACEMENT_ID%]"
        },
        {
            "name":"richload",
            "type":"richload",
            "default":"sprintMerger_160x600_richload/index.html"
        },
        {
            "name":"bgVideo",
            "type":"video",
            "default":"127737/empty_video"
        },
        {
            "name":"bgVideoSizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,unset,#fff"
        },
        {
            "name":"txt_1",
            "type":"text",
            "default":"Sprint Customers,"
        },
        {
            "name":"txt_1_Props",
            "type":"text",
            "default":"21px,normal,#fff,center,0px,280px"
        },
        {
            "name":"txt_2",
            "type":"text",
            "default":"WHY RISK<br>A SERVICE<br>DOWNGRADE?"
        },
        {
            "name":"txt_2_Props",
            "type":"text",
            "default":"20px,23px,#fff,center,0px,341px"
        },
        {
            "name":"txt_3",
            "type":"text",
            "default":"Switch from Sprint"
        },
        {
            "name":"txt_3_Props",
            "type":"text",
            "default":"20px,18px,#FFFFFF,center,0px,113px"
        },
        {
            "name":"txt_4",
            "type":"text",
            "default":"<sup>$</sup>700 OFF"
        },
        {
            "name":"txt_4_Props",
            "type":"text",
            "default":"58px,43px,#FFFFFF,center,0,157px"
        },
        {
            "name":"txt_5",
            "type":"text",
            "default":"latest<span style='width:38px; display:inline-block'>&nbsp;</span>smartphones"
        },
        {
            "name":"txt_5_Props",
            "type":"text",
            "default":"19.5px,normal,#fff,center,0px,255px"
        },
        {
            "name":"txt_6",
            "type":"text",
            "default":"No Activation Fee"
        },
        {
            "name":"txt_6_Props",
            "type":"text",
            "default":"15px,normal,#FFFFFF,center,0px,310px"
        },
        {
            "name":"txt_7",
            "type":"text",
            "default":"UPGRADE<br>TO<br>FAIR"
        },
        {
            "name":"txt_7_Props",
            "type":"text",
            "default":"30px,27px,#fff,center,0px,341px"
        },
        {
            "name":"txt_8",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_8_Props",
            "type":"text",
            "default":"0px,normal,#fff,center,0px,0px"
        },
        {
            "name":"txt_9",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_9_Props",
            "type":"text",
            "default":"0px,normal,#fff,center,0px,0px"
        },
        {
            "name":"txt_10",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_10_Props",
            "type":"text",
            "default":"0px,normal,#fff,center,0px,0px"
        },
        {
            "name":"txt_11",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_11_Props",
            "type":"text",
            "default":"0px,normal,#fff,center,0px,0px"
        },
        {
            "name":"txt_12",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_12_Props",
            "type":"text",
            "default":"0px,normal,#fff,center,0px,0px"
        },
        {
            "name":"svgNode1",
            "type":"text",
            "default":"<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 255 172'><path d='M92,172.1V149.9H18.9c-9,0-16.4-5.6-16.4-12.6V15.3C2.5,8.4,9.9,2.7,18.9,2.7H237.2c9,0,16.4,5.6,16.4,12.6v122c0,6.9-7.4,12.6-16.4,12.6H119.9L92,171' style='fill: none;stroke: #fff;stroke-miterlimit: 10;stroke-width: 3px'/></svg>"
        },
        {
            "name":"svgNode1SizePosZ",
            "type":"text",
            "default":"255px,172px,50px,50px,10"
        },
        {
            "name":"svgNode2",
            "type":"text",
            "default":""
        },
        {
            "name":"svgNode2SizePosZ",
            "type":"text",
            "default":""
        },
        {
            "name":"line_1_SizePosZColor",
            "type":"text",
            "default":"142px,4px,9px,328px,unset,#ee3040"
        },
        {
            "name":"line_2_SizePosZColor",
            "type":"text",
            "default":"126px,4px,17px,301px,unset,#ee3040"
        },
        {
            "name":"line_3_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_4_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_5_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_6_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_7_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_8_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"image_1",
            "type":"image",
            "default":"images/device_samsung_pink.png"
        },
        {
            "name":"image_1SizePosZ",
            "type":"text",
            "default":"111px,174px,0px,0px,1"
        },
        {
            "name":"image_2",
            "type":"image",
            "default":"images/device_samsung_blue.png"
        },
        {
            "name":"image_2SizePosZ",
            "type":"text",
            "default":"111px,174px,0px,0px,1"
        },
        {
            "name":"image_3",
            "type":"image",
            "default":"images/device_samsung_gray.png"
        },
        {
            "name":"image_3SizePosZ",
            "type":"text",
            "default":"111px,174px,0px,0px,1"
        },
        {
            "name":"image_4",
            "type":"image",
            "default":"images/device_galaxys_logo.svg"
        },
        {
            "name":"image_4SizePosZ",
            "type":"text",
            "default":"138px,23px,11px,517px,1"
        },
        {
            "name":"image_5",
            "type":"image",
            "default":"images/logo_5G.svg"
        },
        {
            "name":"image_5SizePosZ",
            "type":"text",
            "default":"31px,20px,95px,254px,unset"
        },
        {
            "name":"image_6",
            "type":"image",
            "default":"images/sprintMerger_160600_f1.jpg"
        },
        {
            "name":"image_6SizePosZ",
            "type":"text",
            "default":"160px,600px,0px,0px,unset"
        },
        {
            "name":"image_7",
            "type":"image",
            "default":"images/tear_160600.png"
        },
        {
            "name":"image_7SizePosZ",
            "type":"text",
            "default":"160px,27px,0px,185px,unset"
        },
        {
            "name":"image_8",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_8SizePosZ",
            "type":"text",
            "default":"300px,250px,0px,0px,1"
        },
        {
            "name": "cta",
            "type": "text",
            "default": "SWITCH NOW"
        },
        {
            "name": "ctaTextProps",
            "type": "text",
            "default": "19px,1em,#FFF,center,0px,1px"
        },
        {
            "name": "ctaSizePosZ",
            "type": "text",
            "default": "140px,40px,10px,541px,unset"
        },
        {
            "name":"addCtaArrow",
            "type":"text",
            "default":"no"
        },
        {
            "name": "uscLogo",
            "type": "image",
            "default": "images/usc-logo-vertical.svg"
        },
        {
            "name": "uscLogoSizePosZ",
            "type": "text",
            "default": "134px,68px,15px,13px,1"
        },
        {
            "name":"legal",
            "type":"text",
            "default":"Terms Apply"
        },
        {
            "name":"legalTextProps",
            "type":"text",
            "default":"9px,10px,#fff,center,0px,0px"
        },
        {
            "name":"legalSizePosZ",
            "type":"text",
            "default":"auto,auto,13px,325px,50"
        },
        {
            "name":"legalDetails",
            "type":"text",
            "default":""
        },
        {
            "name":"legalDetailsZ",
            "type":"text",
            "default":"0"
        },
        {
            "name": "carouselDeviceTotal",
            "type": "text",
            "default": "0"
        },
        {
            "name": "carouselSizePos",
            "type": "text",
            "default": "78px,131px,185px,56px"
        },
        {
            "name": "carouselLogosSizePos",
            "type": "text",
            "default": "76px, 11px, 186px, 188px"
        },
        {
            "name": "carousel_url_1",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_2",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_3",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "showSnipe",
            "type": "text",
            "default": "yes"
        },
        {
            "name": "snipeDate",
            "type": "text",
            "default": "17/08/2020"
        },
        {
            "name": "snipeMaxDays",
            "type": "text",
            "default": "5"
        },
        {
            "name": "snipeText",
            "type": "text",
            "default": "EARLY ACCESS"
        }
        
    ]
});
