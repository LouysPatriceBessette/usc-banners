FT.manifest({
    "filename": "index.html",
    "width": 120,
    "height": 600,
    "clickTagCount": 1,
    "hideBrowsers": ["ie10", "opera"],
    "trackingEvents": [
        {"name": "bannerClick", "type": "string"},
        {"name": "legalClick", "type": "string"}
    ],
    "videos": [
        {"name": "bgVideo", "ref": "127602/BYOD150_120x600"}
    ],
    "richLoads": [
        {"name": "richload", "src": "byop150_120x600_richload"}
    ],
    "instantAds":[
        {
            "name":"creative",
            "type":"text",
            "default":""
        },
        {
            "name":"clickTagURL",
            "type":"text",
            "default":""
        },
        {
            "name":"richload",
            "type":"richload",
            "default":"byop150_120x600_richload/index.html"
        },
        {
            "name":"bgVideo",
            "type":"video",
            "default":"127602/BYOD150_120x600"
        },
        {
            "name":"bgVideoSizePosZColor",
            "type":"text",
            "default":"100%,324px,0px,72px,unset,#fff"
        },
        {
            "name":"txt_1",
            "type":"text",
            "default":"Switch<br>carriers"
        },
        {
            "name":"txt_1_Props",
            "type":"text",
            "default":"20px,24px,#fff,center,0px,453px"
        },
        {
            "name":"txt_2",
            "type":"text",
            "default":"without<br>ever<br>switching<br>phones."
        },
        {
            "name":"txt_2_Props",
            "type":"text",
            "default":"20px,24px,#fff,center,0px,429px"
        },
        {
            "name":"txt_3",
            "type":"text",
            "default":"Bring your<br>own phone"
        },
        {
            "name":"txt_3_Props",
            "type":"text",
            "default":"15.5px,normal,#FFFFFF,center,0px,253px"
        },
        {
            "name":"txt_4",
            "type":"text",
            "default":"GET <span style='font-size:.98em'><sup>$</sup>150</span>"
        },
        {
            "name":"txt_4_Props",
            "type":"text",
            "default": "41.5px,34px,#FFFFFF,center,0px,301px"
        },
        {
            "name":"txt_5",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_5_Props",
            "type":"text",
            "default":"13px,normal,#fff,center,36px,100px"
        },
        {
            "name":"txt_6",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_6_Props",
            "type":"text",
            "default":"16.5px,0px,#fff,center,34px,133px"
        },
        {
            "name":"txt_7",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_7_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_8",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_8_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_9",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_9_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_10",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_10_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_11",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_11_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_12",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_12_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"svgNode1",
            "type":"text",
            "default":"<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 120 600' width='120' height='600'><path d='M50.336,148.76132c0,9.42816-.00052,9.1054.00028,18.157.00026,3.1153.62181,3.73481,3.75022,3.73481,5.96627,0,41.31291.00223,45.83531.00223,6.69715.00424,10.89031,4.19939,10.89093,10.89176.00259,28.26165.003,246.49123-.00036,274.75292-.00082,6.83821-4.13281,10.97684-10.9583,10.9772-45.82231.0024-34.72038.00265-80.54269-.00036-6.42884-.00043-10.66489-4.16842-10.668-10.5624-.01385-28.48117-.0128-246.93025-.00113-275.41144.00266-6.4816,4.23078-10.645,10.77062-10.64692,22.60921-.00657-12.59457.02812,6.21618.02812a5.08117,5.08117,0,0,0,3.90617-1.6237c9.54372-9.46983,9.76429-9.51372,19.34585-18.9454C49.26236,149.73971,49.66418,149.38514,50.336,148.76132Z' style='fill: none;stroke: #fff;stroke-miterlimit: 10;stroke-width: 2.8123030327732588px'/></svg>"
        },
        {
            "name":"svgNode1SizePosZ",
            "type":"text",
            "default":"120px,600px,0px,0px,unset"
        },
        {
            "name":"svgNode2",
            "type":"text",
            "default":""
        },
        {
            "name":"svgNode2SizePosZ",
            "type":"text",
            "default":""
        },
        {
            "name":"line_1_SizePosZColor",
            "type":"text",
            "default":"80px,2.5px,20px,289px,unset,#ee3040"
        },
        {
            "name":"line_2_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_3_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_4_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_5_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_6_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_7_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_8_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"image_1",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_1SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_2",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_2SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_3",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_3SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_4",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_4SizePosZ",
            "type":"text",
            "default":"75px,12px,198px,154px,1"
        },
        {
            "name":"image_5",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_5SizePosZ",
            "type":"text",
            "default":"19px,12px,72px,100px,1"
        },
        {
            "name":"image_6",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_6SizePosZ",
            "type":"text",
            "default":"300px,250px,0px,0px,1"
        },
        {
            "name":"image_7",
            "type":"image",
            "default":"images/logo_5G.svg"
        },
        {
            "name":"image_7SizePosZ",
            "type":"text",
            "default":"19px,12px,72px,100px,1"
        },
        {
            "name":"image_8",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_8SizePosZ",
            "type":"text",
            "default":"300px,250px,0px,0px,1"
        },
        {
            "name": "cta",
            "type": "text",
            "default": "SWITCH NOW"
        },
        {
            "name": "ctaTextProps",
            "type": "text",
            "default": "13.5px,1em,#FFF,center,0px,1px"
        },
        {
            "name": "ctaSizePosZ",
            "type": "text",
            "default": "102px,28px,9px,555px,55"
        },
        {
            "name":"addCtaArrow",
            "type":"text",
            "default":"no"
        },
        {
            "name": "uscLogo",
            "type": "image",
            "default": "images/usc-logo-vertical.svg"
        },
        {
            "name": "uscLogoSizePosZ",
            "type": "text",
            "default": "97px,49px,11px,7px,unset"
        },
        {
            "name":"legal",
            "type":"text",
            "default":"Legal >"
        },
        {
            "name":"legalTextProps",
            "type":"text",
            "default":"9px,10px,#fff,left,0px,0px"
        },
        {
            "name":"legalSizePosZ",
            "type":"text",
            "default":"auto,auto,42px,582px,10"
        },
        {
            "name":"legalDetails",
            "type":"text",
            "default":"Via $5/mo. bill credit. New line, Unlimited Basic, Everyday, or Even Better Plan, compatible smartphone and credit approval required. Taxes, fees, and restrictions apply."
        },
        {
            "name": "carouselDeviceTotal",
            "type": "text",
            "default": "0"
        },
        {
            "name": "carouselSizePos",
            "type": "text",
            "default": "78px,131px,185px,56px"
        },
        {
            "name": "carouselLogosSizePos",
            "type": "text",
            "default": "76px, 11px, 186px, 188px"
        },
        {
            "name": "carousel_url_1",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_2",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_3",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "showSnipe",
            "type": "text",
            "default": "yes"
        },
        {
            "name": "snipeDate",
            "type": "text",
            "default": "17/08/2020"
        },
        {
            "name": "snipeMaxDays",
            "type": "text",
            "default": "5"
        },
        {
            "name": "snipeText",
            "type": "text",
            "default": "EARLY ACCESS"
        }
        
    ]
});
