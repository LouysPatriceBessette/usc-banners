FT.manifest({
    "filename": "index.html",
    "width": 728,
    "height": 90,
    "clickTagCount": 1,
    "hideBrowsers": ["ie10", "opera"],
    "trackingEvents": [
        {"name": "bannerClick", "type": "string"},
        {"name": "legalClick", "type": "string"}
    ],
    "videos": [
        {"name": "bgVideo", "ref": "127602/BYOD150_728x90"}
    ],
    "richLoads": [
        {"name": "richload", "src": "byop150_728x90_richload"}
    ],
    "instantAds":[
        {
            "name":"creative",
            "type":"text",
            "default":""
        },
        {
            "name":"clickTagURL",
            "type":"text",
            "default":""
        },
        {
            "name":"richload",
            "type":"richload",
            "default":"byop150_728x90_richload/index.html"
        },
        {
            "name":"bgVideo",
            "type":"video",
            "default":"127602/BYOD150_728x90"
        },
        {
            "name":"bgVideoSizePosZColor",
            "type":"text",
            "default":"203px,100%,525px,0px,1,#fff"
        },
        {
            "name":"txt_1",
            "type":"text",
            "default":"Switch carriers"
        },
        {
            "name":"txt_1_Props",
            "type":"text",
            "default":"21px,normal,#fff,center,291px,35px"
        },
        {
            "name":"txt_2",
            "type":"text",
            "default":"without ever<br>switching phones."
        },
        {
            "name":"txt_2_Props",
            "type":"text",
            "default":"21px,29px,#fff,center,275px,19px"
        },
        {
            "name":"txt_3",
            "type":"text",
            "default":"Bring your<br>own phone"
        },
        {
            "name":"txt_3_Props",
            "type":"text",
            "default":"22px,normal,#FFFFFF,center,282px,25px"
        },
        {
            "name":"txt_4",
            "type":"text",
            "default":"GET <sup>$</sup>150"
        },
        {
            "name":"txt_4_Props",
            "type":"text",
            "default":"54px,normal,#FFFFFF,center,417px,22px"
        },
        {
            "name":"txt_5",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_5_Props",
            "type":"text",
            "default":"13px,normal,#fff,center,36px,100px"
        },
        {
            "name":"txt_6",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_6_Props",
            "type":"text",
            "default":"16.5px,0px,#fff,center,34px,133px"
        },
        {
            "name":"txt_7",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_7_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_8",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_8_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_9",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_9_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_10",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_10_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_11",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_11_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_12",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_12_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"svgNode1",
            "type":"text",
            "default":"<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 728 90' width='728' height='90'><path d='M208.37644,27.16191c6.28377,0,12.31656.00034,18.34934-.00018,2.07631-.00018,2.48921-.41347,2.48921-2.49365,0-3.96716.00149-4.19929.00149-7.20638.00283-4.45315,2.79885-7.24132,7.25925-7.24173,18.8361-.00171,428.25909-.002,447.0952.00025,4.55759.00054,7.316,2.748,7.31619,7.28653.00159,30.46873.00176,26.31179-.00025,56.78054-.00029,4.27474-2.77821,7.09142-7.03976,7.0935-18.9824.0092-428.55169.00851-447.53409.00074-4.31992-.00176-7.09479-2.81318-7.09606-7.16173-.00438-15.0336.01875-11.88826.01875-24.39613a3.37474,3.37474,0,0,0-1.08218-2.59734q-9.46732-9.51889-18.87482-19.097C209.02853,27.8758,208.79223,27.60862,208.37644,27.16191Z' style='fill: none;stroke: #fff;stroke-miterlimit: 10;stroke-width: 3.3283198033334984px'/></svg>"
        },
        {
            "name":"svgNode1SizePosZ",
            "type":"text",
            "default":"728px,90px,0px,0px,unset"
        },
        {
            "name":"svgNode2",
            "type":"text",
            "default":""
        },
        {
            "name":"svgNode2SizePosZ",
            "type":"text",
            "default":""
        },
        {
            "name":"line_1_SizePosZColor",
            "type":"text",
            "default":"3px,46px,404px,23px,unset,#ee3040"
        },
        {
            "name":"line_2_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_3_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_4_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_5_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_6_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_7_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_8_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"image_1",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_1SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_2",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_2SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_3",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_3SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_4",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_4SizePosZ",
            "type":"text",
            "default":"75px,12px,198px,154px,1"
        },
        {
            "name":"image_5",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_5SizePosZ",
            "type":"text",
            "default":"19px,12px,72px,100px,1"
        },
        {
            "name":"image_6",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_6SizePosZ",
            "type":"text",
            "default":"300px,250px,0px,0px,1"
        },
        {
            "name":"image_7",
            "type":"image",
            "default":"images/logo_5G.svg"
        },
        {
            "name":"image_7SizePosZ",
            "type":"text",
            "default":"19px,12px,72px,100px,1"
        },
        {
            "name":"image_8",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_8SizePosZ",
            "type":"text",
            "default":"300px,250px,0px,0px,1"
        },
        {
            "name": "cta",
            "type": "text",
            "default": "SWITCH NOW"
        },
        {
            "name": "ctaTextProps",
            "type": "text",
            "default": "14px,1em,#FFF,center,0px,1px"
        },
        {
            "name": "ctaSizePosZ",
            "type": "text",
            "default": "104px,29px,43px,54px,55"
        },
        {
            "name":"addCtaArrow",
            "type":"text",
            "default":"no"
        },
        {
            "name": "uscLogo",
            "type": "image",
            "default": "images/uscc-logo.svg"
        },
        {
            "name": "uscLogoSizePosZ",
            "type": "text",
            "default": "162px,40px,15px,7px,7"
        },
        {
            "name":"legal",
            "type":"text",
            "default":"Legal >"
        },
        {
            "name":"legalTextProps",
            "type":"text",
            "default":"9px,10px,#fff,left,0px,0px"
        },
        {
            "name":"legalSizePosZ",
            "type":"text",
            "default":"auto,auto,149px,70px,50"
        },
        {
            "name":"legalDetails",
            "type":"text",
            "default":"Via $5/mo. bill credit. New line, Unlimited Basic, Everyday, or Even Better Plan, compatible smartphone and credit approval required. Taxes, fees, and restrictions apply."
        },
        
        {
            "name": "carouselDeviceTotal",
            "type": "text",
            "default": "0"
        },
        {
            "name": "carouselSizePos",
            "type": "text",
            "default": "78px,131px,185px,56px"
        },
        {
            "name": "carouselLogosSizePos",
            "type": "text",
            "default": "76px, 11px, 186px, 188px"
        },
        {
            "name": "carousel_url_1",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_2",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_3",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "showSnipe",
            "type": "text",
            "default": "yes"
        },
        {
            "name": "snipeDate",
            "type": "text",
            "default": "17/08/2020"
        },
        {
            "name": "snipeMaxDays",
            "type": "text",
            "default": "5"
        },
        {
            "name": "snipeText",
            "type": "text",
            "default": "EARLY ACCESS"
        }
        
    ]
});
