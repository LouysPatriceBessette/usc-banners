FT.manifest({
    "filename": "index.html",
    "width": 320,
    "height": 480,
    "clickTagCount": 1,
    "hideBrowsers": ["ie10", "opera"],
    "trackingEvents": [
        {"name": "bannerClick", "type": "string"},
        {"name": "legalClick", "type": "string"}
    ],
    "videos": [
        {"name": "bgVideo", "ref": "127602/BYOD150_320x480"}
    ],
    "richLoads": [
        {"name": "richload", "src": "byop150_320x480_richload"}
    ],
    "instantAds":[
        {
            "name":"creative",
            "type":"text",
            "default":""
        },
        {
            "name":"clickTagURL",
            "type":"text",
            "default":""
        },
        {
            "name":"richload",
            "type":"richload",
            "default":"byop150_320x480_richload/index.html"
        },
        {
            "name":"bgVideo",
            "type":"video",
            "default":"127602/BYOD150_320x480"
        },
        {
            "name":"bgVideoSizePosZColor",
            "type":"text",
            "default":"100%,401px,0px,0px,unset,#fff"
        },
        {
            "name":"txt_1",
            "type":"text",
            "default":"Switch carriers"
        },
        {
            "name":"txt_1_Props",
            "type":"text",
            "default":"34px,normal,#0064a3,center,0px,354px"
        },
        {
            "name":"txt_2",
            "type":"text",
            "default":"without ever<br>switching phones."
        },
        {
            "name":"txt_2_Props",
            "type":"text",
            "default":"29px,33px,#0064a3,center,0px,335px"
        },
        {
            "name":"txt_3",
            "type":"text",
            "default":"Bring your<br>own phone"
        },
        {
            "name":"txt_3_Props",
            "type":"text",
            "default":"30px,normal,#FFFFFF,center,0px,84px"
        },
        {
            "name":"txt_4",
            "type":"text",
            "default":"GET <sup>$</sup>150"
        },
        {
            "name":"txt_4_Props",
            "type":"text",
            "default":"109px,89px,#FFFFFF,center,-5px,181px"
        },
        {
            "name":"txt_5",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_5_Props",
            "type":"text",
            "default":"13px,normal,#fff,center,36px,100px"
        },
        {
            "name":"txt_6",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_6_Props",
            "type":"text",
            "default":"16.5px,0px,#fff,center,34px,133px"
        },
        {
            "name":"txt_7",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_7_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_8",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_8_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_9",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_9_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_10",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_10_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_11",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_11_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_12",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_12_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"svgNode1",
            "type":"text",
            "default":"<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 320 480' width='320' height='480'><path d='M97.77507,404.36192c0-6.67.00036-21.91625-.0002-28.31986-.00019-2.20394-.43991-2.642-2.65313-2.64222-7.49287-.00063-50.35618.00316-57.84906-.00159-4.73795-.003-7.70444-2.97089-7.70487-7.70546-.00183-19.99394-.00215-276.13808.00026-296.132.00058-4.83775,2.92378-7.76566,7.75254-7.76591,32.41736-.0017,210.258-.00187,242.67535.00026,4.54814.0003,7.545,2.949,7.54717,7.47248.00979,20.14924.00905,276.44866.00079,296.5979-.00188,4.58546-2.9931,7.5309-7.61976,7.53225-15.99507.00465-133.20039.0122-149.19539-.0199a3.59467,3.59467,0,0,0-2.76345,1.14869c-6.75178,6.69952-22.3824,22.20513-29.16094,28.87765C98.53462,403.66975,98.25035,403.92057,97.77507,404.36192Z' style='fill: none;stroke: #fff;stroke-miterlimit: 10;stroke-width: 5.305564826936937px'/></svg>"
        },
        {
            "name":"svgNode1SizePosZ",
            "type":"text",
            "default":"320px,480px,0px,0px,unset"
        },
        {
            "name":"svgNode2",
            "type":"text",
            "default":""
        },
        {
            "name":"svgNode2SizePosZ",
            "type":"text",
            "default":""
        },
        {
            "name":"line_1_SizePosZColor",
            "type":"text",
            "default":"204px,5.5px,56px,159px,unset,#ee3040"
        },
        {
            "name":"line_2_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_3_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_4_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_5_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_6_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_7_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_8_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"image_1",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_1SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_2",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_2SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_3",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_3SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_4",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_4SizePosZ",
            "type":"text",
            "default":"75px,12px,198px,154px,1"
        },
        {
            "name":"image_5",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_5SizePosZ",
            "type":"text",
            "default":"19px,12px,72px,100px,1"
        },
        {
            "name":"image_6",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_6SizePosZ",
            "type":"text",
            "default":"300px,250px,0px,0px,1"
        },
        {
            "name":"image_7",
            "type":"image",
            "default":"images/logo_5G.svg"
        },
        {
            "name":"image_7SizePosZ",
            "type":"text",
            "default":"19px,12px,72px,100px,1"
        },
        {
            "name":"image_8",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_8SizePosZ",
            "type":"text",
            "default":"300px,250px,0px,0px,1"
        },
        {
            "name": "cta",
            "type": "text",
            "default": "SWITCH NOW"
        },
        {
            "name": "ctaTextProps",
            "type": "text",
            "default": "15px,1em,#FFF,center,0px,1px"
        },
        {
            "name": "ctaSizePosZ",
            "type": "text",
            "default": "123px,27px,177px,434px,55"
        },
        {
            "name":"addCtaArrow",
            "type":"text",
            "default":"no"
        },
        {
            "name": "uscLogo",
            "type": "image",
            "default": "images/uscc-logo.svg"
        },
        {
            "name": "uscLogoSizePosZ",
            "type": "text",
            "default": "145px,36px,17px,430px,unset"
        },
        {
            "name":"legal",
            "type":"text",
            "default":"Legal >"
        },
        {
            "name":"legalTextProps",
            "type":"text",
            "default":"9px,10px,#fff,left,0px,0px"
        },
        {
            "name":"legalSizePosZ",
            "type":"text",
            "default":"auto,auto,221px,460px,50"
        },
        {
            "name":"legalDetails",
            "type":"text",
            "default":"Via $5/mo. bill credit. New line, Unlimited Basic, Everyday, or Even Better Plan, compatible smartphone and credit approval required. Taxes, fees, and restrictions apply."
        },
        
        {
            "name": "carouselDeviceTotal",
            "type": "text",
            "default": "0"
        },
        {
            "name": "carouselSizePos",
            "type": "text",
            "default": "78px,131px,185px,56px"
        },
        {
            "name": "carouselLogosSizePos",
            "type": "text",
            "default": "76px, 11px, 186px, 188px"
        },
        {
            "name": "carousel_url_1",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_2",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_3",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "showSnipe",
            "type": "text",
            "default": "yes"
        },
        {
            "name": "snipeDate",
            "type": "text",
            "default": "17/08/2020"
        },
        {
            "name": "snipeMaxDays",
            "type": "text",
            "default": "5"
        },
        {
            "name": "snipeText",
            "type": "text",
            "default": "EARLY ACCESS"
        }
        
    ]
});
