FT.manifest({
    "filename": "index.html",
    "width": 300,
    "height": 250,
    "clickTagCount": 1,
    "hideBrowsers": ["ie10", "opera"],
    "trackingEvents": [
        {"name": "bannerClick", "type": "string"},
        {"name": "legalClick", "type": "string"}
    ],
    "videos": [
        {"name": "bgVideo", "ref": "127602/BYOD150_300x250"}
    ],
    "richLoads": [
        {"name": "richload", "src": "byop150_300x250_richload"}
    ],
    "instantAds":[
        {
            "name":"creative",
            "type":"text",
            "default":""
        },
        {
            "name":"clickTagURL",
            "type":"text",
            "default":""
        },
        {
            "name":"richload",
            "type":"richload",
            "default":"byop150_300x250_richload/index.html"
        },
        {
            "name":"bgVideo",
            "type":"video",
            "default":"127602/BYOD150_300x250"
        },
        {
            "name":"bgVideoSizePosZColor",
            "type":"text",
            "default":"100%,192px,0px,0px,unset,#fff"
        },
        {
            "name":"txt_1",
            "type":"text",
            "default":"Switch carriers"
        },
        {
            "name":"txt_1_Props",
            "type":"text",
            "default":"26px,normal,#0064a3,center,0px,131px"
        },
        {
            "name":"txt_2",
            "type":"text",
            "default":"without ever<br>switching phones."
        },
        {
            "name":"txt_2_Props",
            "type":"text",
            "default":"26px,normal,#0064a3,center,0px,118px"
        },
        {
            "name":"txt_3",
            "type":"text",
            "default":"Bring your own phone"
        },
        {
            "name":"txt_3_Props",
            "type":"text",
            "default":"19.5px,normal,#FFFFFF,center,52px,65px"
        },
        {
            "name":"txt_4",
            "type":"text",
            "default":"GET <sup>$</sup>150"
        },
        {
            "name":"txt_4_Props",
            "type":"text",
            "default":"49px,normal,#FFFFFF,center,48px,100px"
        },
        {
            "name":"txt_5",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_5_Props",
            "type":"text",
            "default":"13px,normal,#fff,center,36px,100px"
        },
        {
            "name":"txt_6",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_6_Props",
            "type":"text",
            "default":"16.5px,0px,#fff,center,34px,133px"
        },
        {
            "name":"txt_7",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_7_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_8",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_8_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_9",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_9_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_10",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_10_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_11",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_11_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_12",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_12_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"svgNode1",
            "type":"text",
            "default":"<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 300 250'><path d='M95.60747,180.70926c0-6.26892.00034-12.28745-.00019-18.306-.00018-2.0714-.41345-2.48315-2.49358-2.48332-7.04228-.00059-47.328.003-54.37025-.00149-4.453-.00282-7.24112-2.79224-7.24153-7.24209-.00172-18.79159-.002-79.71114.00024-98.50273.00055-4.54683,2.748-7.29866,7.28633-7.2989,30.46792-.00159,189.30307-.00176,219.771.00025,4.27464.00028,7.09124,2.77164,7.09331,7.02311.00921,18.93754.00851,80.00305.00075,98.9406-.00176,4.30971-2.8131,7.078-7.16154,7.07928-15.03319.00437-125.19025.01147-140.22338-.0187a3.37848,3.37848,0,0,0-2.59727,1.07962q-9.51864,9.44494-19.09647,18.83021C96.32134,180.05871,96.05416,180.29445,95.60747,180.70926Z' style='fill: none;stroke: #fff;stroke-miterlimit: 10;stroke-width: 2.6272536379703415px'/></svg>"
        },
        {
            "name":"svgNode1SizePosZ",
            "type":"text",
            "default":"300px,250px,0px,0px,unset"
        },
        {
            "name":"svgNode2",
            "type":"text",
            "default":""
        },
        {
            "name":"svgNode2SizePosZ",
            "type":"text",
            "default":""
        },
        {
            "name":"line_1_SizePosZColor",
            "type":"text",
            "default":"203px,2.5px,48px,92px,unset,#ee3040"
        },
        {
            "name":"line_2_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_3_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_4_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_5_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_6_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_7_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_8_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"image_1",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_1SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_2",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_2SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_3",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_3SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_4",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_4SizePosZ",
            "type":"text",
            "default":"75px,12px,198px,154px,1"
        },
        {
            "name":"image_5",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_5SizePosZ",
            "type":"text",
            "default":"19px,12px,72px,100px,1"
        },
        {
            "name":"image_6",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_6SizePosZ",
            "type":"text",
            "default":"300px,250px,0px,0px,1"
        },
        {
            "name":"image_7",
            "type":"image",
            "default":"images/logo_5G.svg"
        },
        {
            "name":"image_7SizePosZ",
            "type":"text",
            "default":"19px,12px,72px,100px,1"
        },
        {
            "name":"image_8",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_8SizePosZ",
            "type":"text",
            "default":"300px,250px,0px,0px,1"
        },
        {
            "name": "cta",
            "type": "text",
            "default": "SWITCH NOW"
        },
        {
            "name": "ctaTextProps",
            "type": "text",
            "default": "15px,1em,#FFF,center,0px,1px"
        },
        {
            "name": "ctaSizePosZ",
            "type": "text",
            "default": "115px,25px,166px,207px,55"
        },
        {
            "name":"addCtaArrow",
            "type":"text",
            "default":"no"
        },
        {
            "name": "uscLogo",
            "type": "image",
            "default": "images/uscc-logo.svg"
        },
        {
            "name": "uscLogoSizePosZ",
            "type": "text",
            "default": "135px,45px,17px,203px,unset"
        },
        {
            "name":"legal",
            "type":"text",
            "default":"Legal >"
        },
        {
            "name":"legalTextProps",
            "type":"text",
            "default":"9px,10px,#fff,left,0px,0px"
        },
        {
            "name":"legalSizePosZ",
            "type":"text",
            "default":"auto,auto,206px,231px,50"
        },
        {
            "name":"legalDetails",
            "type":"text",
            "default":"Via $5/mo. bill credit. New line, Unlimited Basic, Everyday, or Even Better Plan, compatible smartphone and credit approval required. Taxes, fees, and restrictions apply."
        },
        
        {
            "name": "carouselDeviceTotal",
            "type": "text",
            "default": "0"
        },
        {
            "name": "carouselSizePos",
            "type": "text",
            "default": "78px,131px,185px,56px"
        },
        {
            "name": "carouselLogosSizePos",
            "type": "text",
            "default": "76px, 11px, 186px, 188px"
        },
        {
            "name": "carousel_url_1",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_2",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_3",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "showSnipe",
            "type": "text",
            "default": "yes"
        },
        {
            "name": "snipeDate",
            "type": "text",
            "default": "17/08/2020"
        },
        {
            "name": "snipeMaxDays",
            "type": "text",
            "default": "5"
        },
        {
            "name": "snipeText",
            "type": "text",
            "default": "EARLY ACCESS"
        }
        
    ]
});
