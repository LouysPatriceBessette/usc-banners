FT.manifest({
    "filename": "index.html",
    "width": 160,
    "height": 600,
    "clickTagCount": 1,
    "hideBrowsers": ["ie10", "opera"],
    "trackingEvents": [
        {"name": "bannerClick", "type": "string"},
        {"name": "legalClick", "type": "string"}
    ],
    "videos": [
        {"name": "bgVideo", "ref": "127602/BYOD150_160x600"}
    ],
    "richLoads": [
        {"name": "richload", "src": "byop150_160x600_richload"}
    ],
    "instantAds":[
        {
            "name":"creative",
            "type":"text",
            "default":""
        },
        {
            "name":"clickTagURL",
            "type":"text",
            "default":""
        },
        {
            "name":"richload",
            "type":"richload",
            "default":"byop150_160x600_richload/index.html"
        },
        {
            "name":"bgVideo",
            "type":"video",
            "default":"127602/BYOD150_160x600"
        },
        {
            "name":"bgVideoSizePosZColor",
            "type":"text",
            "default":"100%,309px,0px,97px,unset,#fff"
        },
        {
            "name":"txt_1",
            "type":"text",
            "default":"Switch<br>carriers"
        },
        {
            "name":"txt_1_Props",
            "type":"text",
            "default":"24px,28px,#fff,center,0px,446px"
        },
        {
            "name":"txt_2",
            "type":"text",
            "default":"without ever<br>switching<br>phones."
        },
        {
            "name":"txt_2_Props",
            "type":"text",
            "default":"24px,28px,#fff,center,0px,430px"
        },
        {
            "name":"txt_3",
            "type":"text",
            "default":"Bring your<br>own phone"
        },
        {
            "name":"txt_3_Props",
            "type":"text",
            "default":"21.5px,normal,#FFFFFF,center,0px,237px"
        },
        {
            "name":"txt_4",
            "type":"text",
            "default":"GET <sup>$</sup>150"
        },
        {
            "name":"txt_4_Props",
            "type":"text",
            "default":"56px,47px,#FFFFFF,center,-4px,300px"
        },
        {
            "name":"txt_5",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_5_Props",
            "type":"text",
            "default":"13px,normal,#fff,center,36px,100px"
        },
        {
            "name":"txt_6",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_6_Props",
            "type":"text",
            "default":"16.5px,0px,#fff,center,34px,133px"
        },
        {
            "name":"txt_7",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_7_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_8",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_8_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_9",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_9_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_10",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_10_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_11",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_11_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_12",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_12_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"svgNode1",
            "type":"text",
            "default":"<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 160 600'><path d='M66.32236,119.66744c0,12.71886-.0007,12.28345.00038,24.49429.00036,4.20263.83884,5.03837,5.05916,5.03837,8.04867,0,55.73233.003,61.83317.003,9.03466.00572,14.69136,5.6651,14.69218,14.6933.00349,38.12581.0041,270.32633-.00048,308.45214-.00112,9.22495-5.57528,14.80808-14.78308,14.80856-61.81564.00323-46.83881.00357-108.65445-.0005-8.6727-.00057-14.38725-5.62331-14.39145-14.249-.01868-38.42192-.01727-270.91856-.00152-309.3405.00358-8.74387,5.70744-14.36043,14.52988-14.363,30.50049-.00887-16.99044.03794,8.38581.03794a6.85457,6.85457,0,0,0,5.26953-2.19041c12.87476-12.77509,13.17232-12.83429,26.09812-25.55791C64.874,120.98732,65.41607,120.509,66.32236,119.66744Z' style='fill: none;stroke: #fff;stroke-miterlimit: 10;stroke-width: 3.7938795540503607px'/></svg>"
        },
        {
            "name":"svgNode1SizePosZ",
            "type":"text",
            "default":"160px,600px,0px,0px,unset"
        },
        {
            "name":"svgNode2",
            "type":"text",
            "default":""
        },
        {
            "name":"svgNode2SizePosZ",
            "type":"text",
            "default":""
        },
        {
            "name":"line_1_SizePosZColor",
            "type":"text",
            "default":"108px,2.5px,24px,286px,unset,#ee3040"
        },
        {
            "name":"line_2_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_3_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_4_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_5_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_6_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_7_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_8_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"image_1",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_1SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_2",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_2SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_3",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_3SizePosZ",
            "type":"text",
            "default":"284px,451px,0px,0px,1"
        },
        {
            "name":"image_4",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_4SizePosZ",
            "type":"text",
            "default":"75px,12px,198px,154px,1"
        },
        {
            "name":"image_5",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_5SizePosZ",
            "type":"text",
            "default":"19px,12px,72px,100px,1"
        },
        {
            "name":"image_6",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_6SizePosZ",
            "type":"text",
            "default":"300px,250px,0px,0px,1"
        },
        {
            "name":"image_7",
            "type":"image",
            "default":"images/logo_5G.svg"
        },
        {
            "name":"image_7SizePosZ",
            "type":"text",
            "default":"19px,12px,72px,100px,1"
        },
        {
            "name":"image_8",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_8SizePosZ",
            "type":"text",
            "default":"300px,250px,0px,0px,1"
        },
        {
            "name": "cta",
            "type": "text",
            "default": "SWITCH NOW"
        },
        {
            "name": "ctaTextProps",
            "type": "text",
            "default": "18px,1em,#FFF,center,0px,1px"
        },
        {
            "name": "ctaSizePosZ",
            "type": "text",
            "default": "134px,37px,13px,538px,55"
        },
        {
            "name":"addCtaArrow",
            "type":"text",
            "default":"no"
        },
        {
            "name": "uscLogo",
            "type": "image",
            "default": "images/usc-logo-vertical.svg"
        },
        {
            "name": "uscLogoSizePosZ",
            "type": "text",
            "default": "135px,70px,15px,14px,unset"
        },
        {
            "name":"legal",
            "type":"text",
            "default":"Legal >"
        },
        {
            "name":"legalTextProps",
            "type":"text",
            "default":"9px,10px,#fff,left,0px,0px"
        },
        {
            "name":"legalSizePosZ",
            "type":"text",
            "default":"auto,auto,64px,577px,50"
        },
        {
            "name":"legalDetails",
            "type":"text",
            "default":"Via $5/mo. bill credit. New line, Unlimited Basic, Everyday, or Even Better Plan, compatible smartphone and credit approval required. Taxes, fees, and restrictions apply."
        },
        
        {
            "name": "carouselDeviceTotal",
            "type": "text",
            "default": "0"
        },
        {
            "name": "carouselSizePos",
            "type": "text",
            "default": "78px,131px,185px,56px"
        },
        {
            "name": "carouselLogosSizePos",
            "type": "text",
            "default": "76px, 11px, 186px, 188px"
        },
        {
            "name": "carousel_url_1",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_2",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_3",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "showSnipe",
            "type": "text",
            "default": "yes"
        },
        {
            "name": "snipeDate",
            "type": "text",
            "default": "17/08/2020"
        },
        {
            "name": "snipeMaxDays",
            "type": "text",
            "default": "5"
        },
        {
            "name": "snipeText",
            "type": "text",
            "default": "EARLY ACCESS"
        }
        
    ]
});
