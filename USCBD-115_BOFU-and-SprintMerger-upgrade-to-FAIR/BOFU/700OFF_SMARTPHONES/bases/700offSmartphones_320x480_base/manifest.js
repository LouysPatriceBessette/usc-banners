FT.manifest({
    "filename": "index.html",
    "width": 320,
    "height": 480,
    "clickTagCount": 1,
    "hideBrowsers": ["ie10", "opera"],
    "trackingEvents": [
        {"name": "bannerClick", "type": "string"},
        {"name": "legalClick", "type": "string"}
    ],
    "videos": [
        {"name": "bgVideo", "ref": "127602/700OffCanvas_Smartphones_320x480"}
    ],
    "richLoads": [
        {"name": "richload", "src": "700offSmartphones_320x480_richload"}
    ],
    "instantAds":[
        {
            "name":"creative",
            "type":"text",
            "default":""
        },
        {
            "name":"clickTagURL",
            "type":"text",
            "default":""
        },
        {
            "name":"richload",
            "type":"richload",
            "default":"700offSmartphones_320x480_richload/index.html"
        },
        {
            "name":"bgVideo",
            "type":"video",
            "default":"127602/700OffCanvas_Smartphones_320x480"
        },
        {
            "name":"bgVideoSizePosZColor",
            "type":"text",
            "default":"100%,401px,0px,0px,unset,#fff"
        },
        {
            "name":"txt_1",
            "type":"text",
            "default":"$700 off the<br>latest smartphones."
        },
        {
            "name":"txt_1_Props",
            "type":"text",
            "default":"26px,31px,#0064a3,center,0px,331px"
        },
        {
            "name":"txt_2",
            "type":"text",
            "default":"Never pay an<br>activation fee."
        },
        {
            "name":"txt_2_Props",
            "type":"text",
            "default":"26px,normal,#0064a3,center,0px,320px"
        },
        {
            "name":"txt_3",
            "type":"text",
            "default":"Switch and get"
        },
        {
            "name":"txt_3_Props",
            "type":"text",
            "default":"22px,normal,#FFFFFF,center,19px,88px"
        },
        {
            "name":"txt_4",
            "type":"text",
            "default":"<sup>$</sup>700<br>OFF"
        },
        {
            "name":"txt_4_Props",
            "type":"text",
            "default":"64px,47px,#FFFFFF,center,19px,126px"
        },
        {
            "name":"txt_5",
            "type":"text",
            "default":"latest<span style='width:35px; display:inline-block'>&nbsp;</span><br>smartphones"
        },
        {
            "name":"txt_5_Props",
            "type":"text",
            "default":"22px,normal,#fff,center,24px,239px"
        },
        {
            "name":"txt_6",
            "type":"text",
            "default":"No Activation Fee"
        },
        {
            "name":"txt_6_Props",
            "type":"text",
            "default":"16.5px,0px,#fff,center,22px,324px"
        },
        {
            "name":"txt_7",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_7_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_8",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_8_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_9",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_9_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_10",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_10_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_11",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_11_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"txt_12",
            "type":"text",
            "default":""
        },
        {
            "name":"txt_12_Props",
            "type":"text",
            "default":"0px,0px,#fff,center,0px,0px"
        },
        {
            "name":"svgNode1",
            "type":"text",
            "default":"<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 255 172'><path d='M92,172.1V149.9H18.9c-9,0-16.4-5.6-16.4-12.6V15.3C2.5,8.4,9.9,2.7,18.9,2.7H237.2c9,0,16.4,5.6,16.4,12.6v122c0,6.9-7.4,12.6-16.4,12.6H119.9L92,171' style='fill: none;stroke: #fff;stroke-miterlimit: 10;stroke-width: 3px'/></svg>"
        },
        {
            "name":"svgNode1SizePosZ",
            "type":"text",
            "default":"255px,172px,50px,50px,10"
        },
        {
            "name":"svgNode2",
            "type":"text",
            "default":""
        },
        {
            "name":"svgNode2SizePosZ",
            "type":"text",
            "default":""
        },
        {
            "name":"line_1_SizePosZColor",
            "type":"text",
            "default":"135px,4px,24px,299px,unset,#ee3040"
        },
        {
            "name":"line_2_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_3_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_4_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_5_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_6_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_7_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"line_8_SizePosZColor",
            "type":"text",
            "default":"0px,0px,0px,0px,0,#fff"
        },
        {
            "name":"image_1",
            "type":"image",
            "default":"images/device_samsung_pink.png"
        },
        {
            "name":"image_1SizePosZ",
            "type":"text",
            "default":"75px,120px,0px,0px,1"
        },
        {
            "name":"image_2",
            "type":"image",
            "default":"images/device_samsung_blue.png"
        },
        {
            "name":"image_2SizePosZ",
            "type":"text",
            "default":"75px,120px,0px,0px,1"
        },
        {
            "name":"image_3",
            "type":"image",
            "default":"images/device_samsung_gray.png"
        },
        {
            "name":"image_3SizePosZ",
            "type":"text",
            "default":"75px,120px,0px,0px,1"
        },
        {
            "name":"image_4",
            "type":"image",
            "default":"images/device_galaxys_logo.svg"
        },
        {
            "name":"image_4SizePosZ",
            "type":"text",
            "default":"118px,19px,184px,292px,1"
        },
        {
            "name":"image_5",
            "type":"image",
            "default":"images/logo_5G.svg"
        },
        {
            "name":"image_5SizePosZ",
            "type":"text",
            "default":"37px,21px,106px,238px,1"
        },
        {
            "name":"image_6",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_6SizePosZ",
            "type":"text",
            "default":"320px,480px,0px,0px,1"
        },
        {
            "name":"image_7",
            "type":"image",
            "default":"images/logo_5G.svg"
        },
        {
            "name":"image_7SizePosZ",
            "type":"text",
            "default":"19px,12px,72px,100px,1"
        },
        {
            "name":"image_8",
            "type":"image",
            "default":"images/empty_image.png"
        },
        {
            "name":"image_8SizePosZ",
            "type":"text",
            "default":"320px,480px,0px,0px,1"
        },
        {
            "name": "cta",
            "type": "text",
            "default": "SWITCH NOW"
        },
        {
            "name": "ctaTextProps",
            "type": "text",
            "default": "15px,1em,#FFF,center,0px,1px"
        },
        {
            "name": "ctaSizePosZ",
            "type": "text",
            "default": "123px,27px,177px,434px,55"
        },
        {
            "name":"addCtaArrow",
            "type":"text",
            "default":"no"
        },
        {
            "name": "uscLogo",
            "type": "image",
            "default": "images/uscc-logo.svg"
        },
        {
            "name": "uscLogoSizePosZ",
            "type": "text",
            "default": "145px,36px,17px,430px,unset"
        },
        {
            "name":"legal",
            "type":"text",
            "default":"Legal >"
        },
        {
            "name":"legalTextProps",
            "type":"text",
            "default":"9px,10px,#00497b,left,0px,0px"
        },
        {
            "name":"legalSizePosZ",
            "type":"text",
            "default":"auto,auto,221px,460px,50"
        },
        {
            "name":"legalDetails",
            "type":"text",
            "default":"Promotional pricing requires an Unlimited Everyday or Even Better plan, new line, port-in, credit approval, qualified Smartphone purchase and comes via monthly bill credit on a 30-mo. RIC. Taxes, fees, and additional restrictions apply"
        },
        
        {
            "name": "carouselDeviceTotal",
            "type": "text",
            "default": "0"
        },
        {
            "name": "carouselSizePos",
            "type": "text",
            "default": "78px,131px,185px,56px"
        },
        {
            "name": "carouselLogosSizePos",
            "type": "text",
            "default": "76px, 11px, 186px, 188px"
        },
        {
            "name": "carousel_url_1",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_1",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_2",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_2",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_url_3",
            "type": "text",
            "default": ""
        },
        {
            "name": "carousel_img_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "carousel_logo_3",
            "type": "image",
            "default": "images/empty_image.png"
        },
        {
            "name": "showSnipe",
            "type": "text",
            "default": "yes"
        },
        {
            "name": "snipeDate",
            "type": "text",
            "default": "17/08/2020"
        },
        {
            "name": "snipeMaxDays",
            "type": "text",
            "default": "5"
        },
        {
            "name": "snipeText",
            "type": "text",
            "default": "EARLY ACCESS"
        }
        
    ]
});
